package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.EmptyException;

import static org.junit.jupiter.api.Assertions.*;

class DesignationTest {

    @DisplayName("create designation - Happy Path")
    @Test
    void setIsDesignationValidHappyPath() {
        //Arrange//Act//Assert
        new Designation("Mercado");
    }

    @DisplayName("create designation - Null")
    @Test
    void setIsDesignationValidNull() {

        //Arrange//Act//Assert
        assertThrows(NullPointerException.class, () -> {
            new Designation(null);
        });
    }

    @DisplayName("create designation - Empty")
    @Test
    void setIsDesignationValidEmpty() {

        //Arrange//Act//Assert
        assertThrows(EmptyException.class, () -> {
            new Designation("");
        });
    }

    @DisplayName("create designation - Override Equals")
    @Test
    void setIsDesignationValidOverrideEquals() {
        //Arrange//Act//Assert
        Designation designationOne = new Designation("Groceries");
        Designation designationTwo = new Designation("Groceries");

        boolean result = designationOne.equals(designationTwo);

        assertTrue(result);
    }

    @DisplayName("create designation - Override Equals False")
    @Test
    void setIsDesignationValidOverrideEqualsFalse() {
        //Arrange//Act//Assert
        Designation designationOne = new Designation("Groceries");
        Designation designationTwo = new Designation("Gym");

        boolean expected = false;
        boolean result = designationOne.equals(designationTwo);

        assertEquals(expected, result);
    }

    @DisplayName("create designation - Override Equals Null")
    @Test
    void setIsDesignationValidOverrideEqualsNull() {
        //Arrange
        Designation designationOne = new Designation("Groceries");

        //Act
        boolean result = designationOne.equals(null);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create designation - Override Equals Same Object")
    @Test
    void setIsDesignationValidOverrideEqualsSameObject() {
        //Arrange
        Designation designationOne = new Designation("Groceries");

        //Act
        boolean result = designationOne.equals(designationOne);

        //Assert
        assertTrue(result);
    }

    @DisplayName("create designation - Override Equals Different Class")
    @Test
    void setIsDesignationValidOverrideEqualsDifferentClass() {
        //Arrange
        Designation designationOne = new Designation("Groceries");
        Name name = new Name("Ludovina");
        //Act
        boolean result = designationOne.equals(name);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create designation - sameValueAs")
    @Test
    void setIsDesignationValidSameValueAs() {
        //Arrange
        Designation designationOne = new Designation("Groceries");
        Designation designationTwo = new Designation("Groceries");

        //Act
        boolean result = designationOne.sameValueAs(designationTwo);

        //Assert
        assertTrue(result);
    }

    @DisplayName("create designation - sameValueAsFalse")
    @Test
    void setIsDesignationValidSameValueAsFalse() {
        //Arrange
        Designation designationOne = new Designation("Groceries");
        Designation designationTwo = new Designation("Gym");

        //Act
        boolean result = designationOne.sameValueAs(designationTwo);

        //Assert
        assertFalse(result);
    }

    @DisplayName("create denomination - sameValueAsNull")
    @Test
    void setIsDenominationValidSameValueAsNull() {
        //Arrange//Act//Assert
        Designation designationOne = new Designation("Groceries");

        boolean result = designationOne.sameValueAs(null);

        assertFalse(result);
    }
}