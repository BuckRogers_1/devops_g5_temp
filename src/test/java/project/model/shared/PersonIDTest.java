package project.model.shared;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonIDTest {
    Email email;
    PersonID personId;

    @BeforeEach
    void setPersonIDBefore() {
        email = new Email("maria@gmail.com");
        personId = new PersonID(email);
    }

    @DisplayName("create personID - Happy Path")
    @Test
    void setEmailHappyPath() {
        personId.setEmail(new Email("mariaS@gmail.com"));
    }

    @DisplayName("create personID - Happy Path")
    @Test
    void setPersonIDHappyPath() {
        //Arrange
        PersonID expectedResult = new PersonID(email);

        // Act
        PersonID result = personId;

        // Assert
        assertEquals(expectedResult, result);
    }

    @DisplayName("create personID - Happy Path")
    @Test
    void setPersonIDNull() {
        //Act// Assert
        assertThrows(NullPointerException.class, () -> {
            PersonID result = new PersonID(null);
        });
    }

    @DisplayName("equals() - Null Object compare")
    @Test
    void equalsSameObject() {
        //Arrange
        PersonID comparePersonId = null;

        //Act
        boolean result = personId.equals(comparePersonId);

        //Assert
        assertFalse(result);
    }

    @DisplayName("equals() - Different Class")
    @Test
    void equalsDifferentClass() {
        //Arrange
        Name testName = new Name("Nuno");

        //Act
        boolean result = personId.equals(testName);

        //Assert
        assertFalse(result);
    }


    @DisplayName("sameValueAs() - Happy Path")
    @Test
    void sameValueAsHappyPath() {
        //Arrange
        PersonID personID = new PersonID(email);

        //Act
        boolean result = personID.sameValueAs(personID);

        //Assert
        assertTrue(result);
    }

    @DisplayName("sameValueAs() - Happy Path")
    @Test
    void sameNull() {
        //Arrange
        PersonID personID = new PersonID(email);
        PersonID personID1 = null;
        //Act
        boolean result = personID.sameValueAs(personID1);

        //Assert
        assertFalse(result);
    }
}