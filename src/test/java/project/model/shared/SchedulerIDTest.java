package project.model.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

class SchedulerIDTest {
    @Test
    @DisplayName("sameValueAs() - False - Para rever quando houver US")
    void sameValueAsFalse() {
        //Arrange
        SchedulerID schedulerTestID = new SchedulerID();
        SchedulerID schedulerTestToCompareID = new SchedulerID();

        //Act
        boolean result = schedulerTestID.sameValueAs(schedulerTestToCompareID);

        //Assert
        assertFalse(result);
    }
}