package project.model.ledger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.account.Account;
import project.model.shared.*;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class TransactionTest {
    Description description;
    Category category;
    AccountID credit;
    AccountID debit;
    GroupID gpID;
    TransactionType transType;

    @BeforeEach()
    void Transaction() {
        double amount = 13.4;
        description = new Description("Barbie");
        category = new Category(new Designation("shopping"));
        Description groupdescription = new Description("Girls Group");
        gpID = new GroupID(groupdescription);
        debit = new AccountID(new Denomination("Banc"), gpID);
        credit = new AccountID(new Denomination("coins"), gpID);
        transType = TransactionType.valueOf("CREDIT");
    }

    @DisplayName("Create transaction - Happy Path")
    @Test
    void crateTransactionHappyPath() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        //Act//Assert
        new Transaction(amount, description, date, category, debit, credit, transType);
    }

    @DisplayName(" NullPointerException")
    @Test
    void transactionNullPointerException() {

        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        ;
        double amount = 13.4;
        //Act//Assert
        assertThrows(NullPointerException.class, () -> new Transaction(amount, null, date, category, debit, credit, transType));
    }

    @DisplayName("no such transaction type Exception")
    @Test
    void transactionTypeInvalidException() {

        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;

        //Act//Assert
        assertThrows(NullPointerException.class, () -> new Transaction(amount, description, date, category, debit, credit, null));
    }

    @DisplayName("The debit Account is the same as the credit one Exception")
    @Test
    void sameCreditAndDebitAccountException() {

        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        AccountID credit = debit;

        //Act//Assert
        assertThrows(IllegalArgumentException.class, () -> new Transaction(amount, description, date, category, debit, credit, transType));
    }

    @DisplayName("getCategory() - Happy Path ")
    @Test
    void getCategory() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        Transaction transaction = new Transaction(amount, description, date, category, debit, credit, transType);
        Category expected = new Category(new Designation("shopping"));
        //Act
        Category result = transaction.getCategory();
        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("getAccountID-credit() - Happy Path ")
    @Test
    void getAccountIDCredit() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        Transaction transaction = new Transaction(amount, description, date, category, debit, credit, transType);
        AccountID expected = new AccountID(new Denomination("coins"), gpID);
        //Act
        AccountID result = transaction.getCredit();
        //Assert
        assertEquals(expected, result);
    }


    @DisplayName("getAccountID-debit() - Happy Path ")
    @Test
    void getAccountIDebit() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        Transaction transaction = new Transaction(amount, description, date, category, debit, credit, transType);
        AccountID expected = new AccountID(new Denomination("Banc"), gpID);
        //Act
        AccountID result = transaction.getDebit();
        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("get type transaction - Happy Path ")
    @Test
    void getTypeTransactic() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        Transaction transaction = new Transaction(amount, description, date, category, debit, credit, transType);
        TransactionType expected = TransactionType.valueOf("CREDIT");
        //Act
        TransactionType result = transaction.getType();
        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("get type description - Happy Path ")
    @Test
    void getDescription() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        Transaction transaction = new Transaction(amount, description, date, category, debit, credit, transType);
        Description expected = new Description("Barbie");
        //Act
        Description result = transaction.getDescription();
        //Assert
        assertEquals(expected, result);
    }


    @DisplayName("Equals override - Same Object")
    @Test
    void testEqualsSameObject() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 12.3;
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        // Act
        boolean result = t01.equals(t01);
        //Assert
        assertTrue(result);
    }

    @DisplayName("Equals override - Different Objects with the same attributes")
    @Test
    void testEqualsSameAttributesDifObject() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 12.3;
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        Transaction t02 = new Transaction(amount, description, date, category, debit, credit, transType);
        //Act
        boolean result = t01.equals(t02);
        //Assert
        assertTrue(result);
    }

    @DisplayName("Equals override - Different Object Type")
    @Test
    void testEqualsDifferentObjectType() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 12.3;
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        Account account = new Account(debit, "Description");
        //Act
        boolean result = t01.equals(account);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Equals override - Null Object")
    @Test
    void testEqualsNullObject() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 12.3;
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        Transaction t02 = null;
        //Act
        boolean result = t01.equals(t02);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Hash Code Override test")
    @Test
    void hashCodeTest() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 12.3;
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        Transaction t02 = new Transaction(amount, description, date, category, debit, credit, transType);

        int expectedResult = t02.hashCode();
        //Act
        int result = t01.hashCode();
        //Assert
        assertEquals(expectedResult, result);
    }

    @DisplayName("Equals override - Different amount")
    @Test
    void testEqualsDifAmount() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amountOther = 56.3;
        double amount = 12.3;
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        Transaction t02 = new Transaction(amountOther, description, date, category, debit, credit, transType);
        //Act
        boolean result = t01.equals(t02);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Equals override - Different description")
    @Test
    void testEqualsDifDescription() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        Description descriptionOther = new Description("Clothes");
        double amount = 13.4;
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        Transaction t02 = new Transaction(amount, descriptionOther, date, category, debit, credit, transType);
        //Act
        boolean result = t01.equals(t02);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Equals override - Different date")
    @Test
    void testEqualsDifDate() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        TransactionDate dateOther = new TransactionDate(LocalDateTime.of(2004, 6, 9, 22, 10, 30));
        double amount = 13.4;
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        Transaction t02 = new Transaction(amount, description, dateOther, category, debit, credit, transType);
        //Act
        boolean result = t01.equals(t02);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Equals override - Different category")
    @Test
    void testEqualsDifCategory() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        Category categoryOther = new Category(new Designation("Street shop"));
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        Transaction t02 = new Transaction(amount, description, date, categoryOther, debit, credit, transType);
        //Act
        boolean result = t01.equals(t02);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Equals override - Different Debit Account")
    @Test
    void testEqualsDifDebitAccount() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        AccountID debitOther = new AccountID(new Denomination("money"), gpID);
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        Transaction t02 = new Transaction(amount, description, date, category, debitOther, credit, transType);
        //Act
        boolean result = t01.equals(t02);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Equals override - Different Credit Account")
    @Test
    void testEqualsDifCreditAccount() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        AccountID creditOther = new AccountID(new Denomination("money"), gpID);
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        Transaction t02 = new Transaction(amount, description, date, category, debit, creditOther, transType);
        //Act
        boolean result = t01.equals(t02);
        //Assert
        assertFalse(result);
    }

    @DisplayName("Equals override - Different TransactionType")
    @Test
    void testEqualsDifTransType() {
        //Arrange
        TransactionDate date = new TransactionDate(LocalDateTime.of(1999, 3, 10, 22, 10, 30));
        double amount = 13.4;
        Description descriptionOther = new Description("Dolls");
        Transaction t01 = new Transaction(amount, description, date, category, debit, credit, transType);
        Transaction t02 = new Transaction(amount, descriptionOther, date, category, debit, credit, transType);
        //Act
        boolean result = t01.equals(t02);
        //Assert
        assertFalse(result);
    }
}

