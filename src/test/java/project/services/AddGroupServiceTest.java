package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddGroupRequestDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.AddGroupRequestAssembler;
import project.exceptions.AlreadyExistsException;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.GroupID;
import project.model.shared.PersonID;
import project.repositories.GroupRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
class AddGroupServiceTest {
    @Autowired
    GroupRepository groupMockRepository;
    @Autowired
    AddGroupService addGroupService;

    Email email;
    Description description;
    PersonID responsible;

    @DisplayName("As a user, I want to create a group, becoming a group administrator -add Group - Happy path ")
    @Test
    void addGroupHappyPath() {
        //Arrange

        description = new Description("Friends and Family");
        email = new Email("maria@gmail.com");
        responsible = new PersonID(email);
        GroupDTO expected = new GroupDTO();
        expected.setGroupID(new GroupID(description));
        AddGroupRequestDTO addGroupRequestDTO = AddGroupRequestAssembler.mapToDTO(description.getDescription(), email.getEmail());

        Group groupOptional = new Group(description, responsible);
        Mockito.when(groupMockRepository.findById(new GroupID(description))).thenReturn(Optional.empty());

        Group returnGroup = new Group(addGroupRequestDTO.getGroupDescription(), addGroupRequestDTO.getPersonID());

        Mockito.when(groupMockRepository.save(returnGroup)).thenReturn(returnGroup);

        //Act
        GroupDTO result = addGroupService.addGroup(addGroupRequestDTO);

        //Assert
        assertEquals(result.getGroupID(), expected.getGroupID());
    }

    @DisplayName("As a user, I want to create a group, but it already exists ")
    @Test
    void addGroupAlreadyExists() {
        //Arrange
        description = new Description("Friends");
        email = new Email("sofia@gmail.com");
        responsible = new PersonID(email);
        GroupDTO groupDTO = new GroupDTO();
        groupDTO.setGroupID(new GroupID(description));
        AddGroupRequestDTO addGroupRequestDTO = AddGroupRequestAssembler.mapToDTO(description.getDescription(), email.getEmail());

        Group groupOptional = new Group(description, responsible);

        Mockito.when(groupMockRepository.findById(new GroupID(description))).thenReturn(Optional.of(groupOptional));

        //Act//Assert
        assertThrows(AlreadyExistsException.class, () -> {
            GroupDTO group = addGroupService.addGroup(new AddGroupRequestDTO(description, responsible));
        });
    }
}


