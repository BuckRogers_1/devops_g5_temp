package project.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GroupDTO;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * US004 - Como gestor quero saber quais os grupos que são família, i.e. grupo constituído por pai, mãe e um sub-conjunto (não vazio,
 * mas podem ser todos) dos respetivos filhos.
 */
@SpringBootTest
@ActiveProfiles("test2")
@ExtendWith(SpringExtension.class)
public class GetFamiliesServiceTest {
    @Autowired
    GetFamiliesService getFamiliesService;
    @Autowired
    PersonRepository personMockRepository;
    @Autowired
    GroupRepository groupMockRepository;

    @DisplayName("getFamilies - No families found")
    @Test
    void getFamiliesNoFamiliesFound() {
        //Arrange
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));

        Name jose = new Name("José");
        Email joseEmail = new Email("josebides@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);

        Mockito.when(personMockRepository.findById(father.getPersonID())).thenReturn(java.util.Optional.of(father));

        // GROUPS
        Description familyDescription = new Description("Good Enough Members For This Family");
        Group familyGroup = new Group(familyDescription, father.getPersonID());
        Set<Group> familyCollection = new HashSet<>();
        familyCollection.add(familyGroup);

        Mockito.when(groupMockRepository.findAll()).thenReturn(familyCollection);

        //Act
        Set<GroupDTO> result = getFamiliesService.getFamilies();

        assertEquals(0, result.size());
    }
}