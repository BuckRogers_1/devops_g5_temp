package project.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GetJsonNodeValueTest {
    @DisplayName("nodeAsString() - Happy Path")
    @Test
    void nodeAsString() {
        //Arrange
        GetJsonNodeValue getJsonNodeValue = new GetJsonNodeValue();

        String jsonText = "{\"groupDescription\":\"test group\",\"responsibles\":\"[maria@gmail.com]\",\"members\":\"[maria@gmail.com]\",\"categories\":\"[]\",\"creationDate\":\"2020-04-28\"}";
        String expected = "test group";
        String result = "";

        //Act
        try {
            result = GetJsonNodeValue.nodeAsString(jsonText, "groupDescription");
        } catch (Exception excp) {
            System.out.println(excp.getMessage());
        }

        //Assert
        assertEquals(expected, result);
    }
}