package project.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.model.group.Group;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.GroupID;
import project.model.shared.PersonID;

import javax.transaction.Transactional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GroupRepositoryTest {
    @Autowired
    GroupRepository groupRepository;
    Description description;
    Email email;
    PersonID responsible;

    @BeforeEach()
    void setUp() {
        description = new Description("Sports for the Family");
        email = new Email("joana@gmail.com");
        responsible = new PersonID(email);
    }

    @DisplayName("save() - Happy Path")
    @Test
    void saveHappyPath() {
        //Arrange
        Group testGroup = new Group(description, responsible);

        //Act
        Group result = groupRepository.save(testGroup);
        System.out.println(result.getID());
        //Assert
        assertEquals(testGroup, result);
    }

    @DisplayName("save() - Null Group")
    @Test
    void saveNullGroup() {
        //Arrange
        Group testGroup = null;

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            Group result = groupRepository.save(testGroup);
        });
    }

    @Transactional
    @DisplayName("update() - Happy Path")
    @Test
    void updateHappyPath() {
        //Arrange
        Group testGroup = new Group(description, responsible);
        PersonID newMember = new PersonID(email);
        testGroup.addMember(newMember);
        Group testGroup2 = new Group(description, responsible);
        groupRepository.save(testGroup2);
        //Act
        Group result = groupRepository.updateMembers(testGroup2, newMember);
        System.out.println(result.getID());

        Set<PersonID> membersResult = result.getMembers();
        Set<PersonID> membersTest = testGroup.getMembers();

        //Assert
        assertEquals(membersTest, membersResult);
    }

    @Transactional
    @DisplayName("update() - Null member")
    @Test
    void updateNullGroup() {
        //Arrange
        groupRepository.deleteAll();
        Group testGroup = new Group(description, responsible);
        groupRepository.save(testGroup);
        PersonID newNullMember = null;

        //Act//Assert
        assertThrows(NullPointerException.class, () -> {
            Group result = groupRepository.updateMembers(testGroup, newNullMember);
        });
    }

    @Transactional
    @DisplayName("findAll() - Happy Path")
    @Test
    void findAll() {
        //Arrange
        final Group[] result = {null};
        Group testGroup = new Group(description, responsible);
        groupRepository.save(testGroup);

        Group expected = new Group(description, responsible);

        //Act
        Iterable<Group> groups = groupRepository.findAll();

        groups.forEach(group -> {
            result[0] = group;
        });

        //Assert
        assertEquals(testGroup, result[0]);
    }

    @Transactional
    @DisplayName("findById() - Happy Path")
    @Test
    void findByIdHappyPath() {
        //Assert
        GroupID groupID = new GroupID(description);
        Group testGroup = new Group(description, responsible);
        Group resultGroup = null;
        groupRepository.save(testGroup);

        //Act
        if (groupRepository.findById(groupID).isPresent()) {
            resultGroup = groupRepository.findById(groupID).get();
        }

        //Assert
        assertEquals(testGroup, resultGroup);
    }

    @DisplayName("findById() - No groups found")
    @Test
    void findById() {
        groupRepository.deleteAll();
        //Assert
        GroupID groupID = new GroupID(description);
        boolean result = true;

        //Act
        if (!groupRepository.findById(groupID).isPresent()) {
            result = false;
        }

        //Assert
        assertFalse(result);
    }
}