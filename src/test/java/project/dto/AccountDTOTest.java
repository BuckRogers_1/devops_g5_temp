package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.AccountAssembler;
import project.model.account.Account;
import project.model.shared.*;

class AccountDTOTest {
    Denomination accountDenomination;
    Description accountDescription;
    Email personEmail;
    PersonID personId;
    Account account;
    AccountDTO accountDTO;

    @BeforeEach
    void setUp() {
        //Arrange
        accountDenomination = new Denomination("Main Bank Account");
        accountDescription = new Description("Generic account for all transactions");

        personEmail = new Email("person@gmail.com");
        personId = new PersonID(personEmail);

        account = new Account(accountDenomination, accountDescription, personId);

        accountDTO = AccountAssembler.mapToDTO(account);
    }

    @Test
    void getDescription() {
        Description result = accountDTO.getDescription();
    }

    @Test
    void setDescription() {
        accountDTO.setDescription(accountDescription);
    }

    @Test
    void getAccountID() {
        AccountID result = accountDTO.getAccountID();
    }

    @Test
    void setAccountID() {
        accountDTO.setAccountID(account.getAccountID());
    }
}