package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.AddAccountToGroupRequestAssembler;
import project.model.shared.*;

import static org.junit.jupiter.api.Assertions.*;

class AddAccountToGroupRequestDTOTest {
    AddAccountToGroupRequestDTO addAccountToGroupRequestDTO;
    Denomination denomination;
    Description description;
    GroupID groupID;
    PersonID personID;
    Email email;
    Description groupDescription;

    @BeforeEach
    void setUp() {
        //Arrange
        denomination = new Denomination("Groceries Account");
        description = new Description("Good");
        email = new Email("gil@gmail.com");
        groupDescription = new Description("Family");
        personID = new PersonID(email);
        groupID = new GroupID(groupDescription);

        addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO("Groceries Account", "Good", "gil@gmail.com", "Family");
    }

    @Test
    void getDenomination() {
        Denomination result = addAccountToGroupRequestDTO.getDenomination();
    }

    @Test
    void setDenomination() {
        addAccountToGroupRequestDTO.setDenomination(denomination);
    }

    @Test
    void getDescription() {
        Description result = addAccountToGroupRequestDTO.getDescription();
    }

    @Test
    void setDescription() {
        addAccountToGroupRequestDTO.setDescription(description);
    }

    @Test
    void getGroupID() {
        GroupID result = addAccountToGroupRequestDTO.getGroupID();
    }

    @Test
    void setGroupID() {
        addAccountToGroupRequestDTO.setGroupID(groupID);
    }

    @Test
    void getPersonID() {
        PersonID result = addAccountToGroupRequestDTO.getPersonID();
    }

    @Test
    void setPersonID() {
        addAccountToGroupRequestDTO.setPersonID(personID);
    }

    @DisplayName("Override Equals - True")
    @Test
    void AddAccountToGroupRequestAssemblerEqualsTrue() {
        //Arrange
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO("Groceries Account", "Good", "gil@gmail.com", "Family");
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTOOther = AddAccountToGroupRequestAssembler.mapToDTO("Groceries Account", "Good", "gil@gmail.com", "Family");
        //Act
        boolean result = addAccountToGroupRequestDTOOther.equals(addAccountToGroupRequestDTO);
        //Assset
        assertTrue(result);
    }

    @DisplayName("Override Equals - same object")
    @Test
    void AddAccountToGroupRequestAssemblerEqualsSameObject() {
        //Arrange
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO("Groceries Account", "Good", "gil@gmail.com", "Family");
        //Act
        boolean result = addAccountToGroupRequestDTO.equals(addAccountToGroupRequestDTO);
        //Assset
        assertTrue(result);
    }

    @DisplayName("test for HashCode")
    @Test
    void testHashCode() {
        AddAccountToGroupRequestDTO expect = new AddAccountToGroupRequestDTO(denomination, description, personID, groupID);

        int expectHash = expect.hashCode();
        int resultHash = addAccountToGroupRequestDTO.hashCode();

        assertEquals(expectHash, resultHash);
    }

    @DisplayName("test for HashCode with fixed value")
    @Test
    void testHashCodeWithFixedValue() {
        int expectHash = -1240602065;

        int resultHash = addAccountToGroupRequestDTO.hashCode();

        assertEquals(expectHash, resultHash);
    }

    @Test
    void testEqualsNull() {
        //Arrage
        AddAccountToGroupRequestDTO nullAccountToGroupRequestDTO = null;
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO("Groceries Account", "Good", "gil@gmail.com", "Family");
        // Act
        boolean result = addAccountToGroupRequestDTO.equals(nullAccountToGroupRequestDTO);
        //Assert
        assertFalse(result);
    }

    @DisplayName(" test Equals - diferent objects")
    @Test
    void testEqualsDifferentObject() {
        //Arrage
        Description otherAccountToGroupRequestDTO = new Description("Shoes");
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO("Groceries Account", "Good", "gil@gmail.com", "Family");
        // Act
        boolean result = addAccountToGroupRequestDTO.equals(otherAccountToGroupRequestDTO);
        //Assert
        assertFalse(result);
    }

    @DisplayName("test Equals - denomination")
    @Test
    void testEqualsDifferentObjectsDenomination() {

        //Arrange
        Denomination denomination = new Denomination("Party");
        Description description = new Description("Jeans");
        Description group = new Description("Girls Club");
        GroupID groupDescription = new GroupID(group);
        Email email = new Email("marta@gmail.com");
        PersonID responsibleEmail = new PersonID(email);
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO1 = new AddAccountToGroupRequestDTO(denomination, description, responsibleEmail, groupDescription);
        //Act
        Denomination denomination2 = new Denomination("Boys Club");
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO2 = new AddAccountToGroupRequestDTO(denomination2, description, responsibleEmail, groupDescription);

        //Assert
        assertFalse(addAccountToGroupRequestDTO1.equals(addAccountToGroupRequestDTO2));
    }

    @DisplayName("test Equals - description")
    @Test
    void testEqualsDifferentObjectsDescription() {

        //Arrange
        Denomination denomination = new Denomination("Party");
        Description description = new Description("Jeans");
        Description group = new Description("Girls Club");
        GroupID groupDescription = new GroupID(group);
        Email email = new Email("marta@gmail.com");
        PersonID responsibleEmail = new PersonID(email);
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO1 = new AddAccountToGroupRequestDTO(denomination, description, responsibleEmail, groupDescription);
        //Act
        Description description2 = new Description("shoes");
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO2 = new AddAccountToGroupRequestDTO(denomination, description2, responsibleEmail, groupDescription);

        //Assert
        assertFalse(addAccountToGroupRequestDTO1.equals(addAccountToGroupRequestDTO2));
    }

    @DisplayName("test Equals - groupdescription")
    @Test
    void testEqualsDifferentObjectsGroupDescription() {

        //Arrange
        Denomination denomination = new Denomination("Party");
        Description description = new Description("Jeans");
        Description group = new Description("Girls Club");
        GroupID groupDescription = new GroupID(group);
        Email email = new Email("marta@gmail.com");
        PersonID responsibleEmail = new PersonID(email);
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO1 = new AddAccountToGroupRequestDTO(denomination, description, responsibleEmail, groupDescription);
        //Act
        Description group2 = new Description("Boys Club");
        GroupID groupDescription2 = new GroupID(group2);
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO2 = new AddAccountToGroupRequestDTO(denomination, description, responsibleEmail, groupDescription2);

        //Assert
        assertFalse(addAccountToGroupRequestDTO1.equals(addAccountToGroupRequestDTO2));
    }

    @DisplayName("test Equals - responsible Email")
    @Test
    void testEqualsDifferentObjectsResponsibleEmail() {

        //Arrange
        Denomination denomination = new Denomination("Party");
        Description description = new Description("Jeans");
        Description group = new Description("Girls Club");
        GroupID groupDescription = new GroupID(group);
        Email email = new Email("marta@gmail.com");
        PersonID responsibleEmail = new PersonID(email);
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO1 = new AddAccountToGroupRequestDTO(denomination, description, responsibleEmail, groupDescription);
        //Act
        Email email2 = new Email("manuel@gmail.com");
        PersonID responsibleEmail2 = new PersonID(email2);
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO2 = new AddAccountToGroupRequestDTO(denomination, description, responsibleEmail2, groupDescription);

        //Assert
        assertFalse(addAccountToGroupRequestDTO1.equals(addAccountToGroupRequestDTO2));
    }
}