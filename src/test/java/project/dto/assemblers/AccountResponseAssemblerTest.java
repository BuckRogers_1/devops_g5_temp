package project.dto.assemblers;

import org.junit.jupiter.api.Test;
import project.dto.AccountDTO;
import project.dto.AccountResponseDTO;
import project.model.shared.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AccountResponseAssemblerTest {

    @Test
    void mapToAccountInfoOutDTOHappyPath() {
        //Arrange
        String denomination = "Sports";
        String description = "Ski";
        String ownerID = "Owner@gmail.com";

        AccountResponseDTO expected = new AccountResponseDTO(denomination, description, ownerID);
        //Act
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setDescription(new Description("Ski"));
        accountDTO.setAccountID(new AccountID(new Denomination("Sports"), new PersonID(new Email("Owner@gmail.com"))));
        AccountResponseDTO result = AccountResponseAssembler.mapToDTO(accountDTO);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    void AccountResponseAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            AccountResponseAssembler accountResponseAssembler = new AccountResponseAssembler();
        });
    }
}