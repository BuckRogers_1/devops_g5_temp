package project.dto.assemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.AddGroupRequestDTO;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AddGroupRequestAssemblerTest {

    @DisplayName("AddGroupRequestAssembler - Constructor Test")
    @Test
    void AddGroupRequestAssembler() {
        String groupDescription = "Test group1";
        String responsibleEmail = "email1@gmail.com";
        AddGroupRequestDTO result = AddGroupRequestAssembler.mapToDTO(groupDescription, responsibleEmail);
        AddGroupRequestDTO expected = new AddGroupRequestDTO(new Description(groupDescription), new PersonID(new Email(responsibleEmail)));
        assertEquals(expected, result);
    }

    @Test
    void AddGroupRequestAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            AddGroupRequestAssembler addGroupRequestAssembler = new AddGroupRequestAssembler();
        });
    }
}
