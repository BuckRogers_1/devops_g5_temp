package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.AddCategoryRequestDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AddCategoryRequestAssemblerTest {

    String designation;
    String groupDescription;
    String responsibleEmail;

    @BeforeEach
    void setUp() {
        designation = "Diamantes";
        groupDescription = "Família Dos Santos";
        responsibleEmail = "isabel@angola.com";
    }

    @Test
    void AddCategoryRequestAssembler() {
        //Act
        AddCategoryRequestDTO expected = AddCategoryRequestAssembler.addCategoryMapToDTO(designation, groupDescription, responsibleEmail);
        AddCategoryRequestDTO result = AddCategoryRequestAssembler.addCategoryMapToDTO(designation, groupDescription, responsibleEmail);
        //Assert
        assertEquals(expected, result);
    }

    @Test
    void AddCategoryRequestAssemblerError() {
        assertThrows(AssertionError.class, AddCategoryRequestAssembler::new);
    }
}