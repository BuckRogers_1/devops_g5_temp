package project.dto.assemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.AddTransactionToGroupRequestDTO;
import project.model.shared.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AddTransactionToGroupRequestAssemblerTest {

    @Test
    @DisplayName("Add transaction To Group Request Assembler Test - Happy path")
    void addTransactionMapToDTOHappyPath() {
        //Arrange
        Denomination denomination = new Denomination("Dolls");
        Description description = new Description("Barbie");
        Description groupdescription = new Description("Girls Group");
        Email memberEmail = new Email("sofia@gmail.com");
        PersonID memberID = new PersonID(memberEmail);
        GroupID gpID = new GroupID(groupdescription);
        double amount = 13.4;
        AccountID debit = new AccountID(new Denomination("Banc"), gpID);
        AccountID credit = new AccountID(new Denomination("coins"), gpID);
        TransactionType transType = TransactionType.valueOf("CREDIT");
        Category category = new Category(new Designation("shopping"));

        AddTransactionToGroupRequestDTO expected = new AddTransactionToGroupRequestDTO(memberID, gpID, amount, description, category, debit, credit, transType);
        //Act

        AddTransactionToGroupRequestDTO result = AddTransactionToGroupRequestAssembler.mapToDTO("sofia@gmail.com", "Girls Group", 13.4, "Barbie", "shopping", "Banc", "coins", "CREDIT");

        //Assert
        assertEquals(expected, result);
    }
}