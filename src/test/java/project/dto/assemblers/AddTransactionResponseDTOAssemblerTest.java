package project.dto.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.dto.AddTransactionResponseDTO;
import project.dto.TransactionDTO;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AddTransactionResponseDTOAssemblerTest {
    TransactionDTO transactionDTO;

    Person mario;
    Email marioEmail;
    Address marioBirthPlace;
    Date marioBirthDate;
    Group group;
    PersonID personID;
    GroupID groupID;
    double amount;
    Description description;
    Category category;
    AccountID debit;
    AccountID credit;
    TransactionType type;
    TransactionDate date;

    @BeforeEach
    void setUp() {
        // Arrange
        marioEmail = new Email("mario@family.com");
        marioBirthPlace = new Address("Porto");
        marioBirthDate = new Date(LocalDateTime.of(1990, Month.JANUARY, 1, 1, 1, 1));
        mario = new Person(new Name("Mario"), marioBirthPlace, marioBirthDate, marioEmail, null, null);
        personID = mario.getPersonID();
        group = new Group(new Description("Family"), mario.getPersonID());
        groupID = group.getID();
        amount = 20.0;
        description = new Description("Lunch");
        category = new Category(new Designation("Market"));
        debit = new AccountID(new Denomination("Pocket"), groupID);
        credit = new AccountID(new Denomination("Restaurante Pinheiro"), groupID);
        type = TransactionType.DEBIT;
        date = new TransactionDate(LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0, 0));

        transactionDTO = new TransactionDTO(amount, description, date, category, debit, credit, type);
    }


    @DisplayName("Add transaction Response DTO Assembler Test - Happy path")
    @Test
    void mapToDTO() {
        //Arrange
        AddTransactionResponseDTO expected = new AddTransactionResponseDTO(20.0, "Lunch", date, "Market", "Pocket", "Restaurante Pinheiro", type);

        //Act
        AddTransactionResponseDTO result = AddTransactionResponseDTOAssembler.mapToDTO(transactionDTO);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void addTransactionResponseDTOAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            AddTransactionResponseDTOAssembler addTransactionResponseDTOAssembler = new AddTransactionResponseDTOAssembler();
        });
    }
}