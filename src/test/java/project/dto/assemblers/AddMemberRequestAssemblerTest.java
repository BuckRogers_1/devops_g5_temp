package project.dto.assemblers;

import org.junit.jupiter.api.Test;
import project.dto.AddMemberRequestDTO;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.GroupID;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AddMemberRequestAssemblerTest {
    @Test
    void addMemberMapToDTO() {
        //Arrange
        GroupID groupID = new GroupID(new Description("Family"));
        PersonID personID = new PersonID(new Email("pedro@gmail.com"));
        AddMemberRequestDTO expected = new AddMemberRequestDTO(groupID, personID);

        //Act
        AddMemberRequestDTO result = AddMemberRequestAssembler.mapToDTO("Family", "pedro@gmail.com");

        //Assert
        assertEquals(expected.getGroupID(), result.getGroupID());
        assertEquals(expected.getPersonID(), result.getPersonID());
    }

    @Test
    void AddMemberRequestAssemblerError() throws Exception {

        assertThrows(AssertionError.class, () -> {
            AddMemberRequestAssembler addMemberRequestAssembler = new AddMemberRequestAssembler();
        });
    }
}
