package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.AddAccountToPersonRequestAssembler;
import project.model.shared.Denomination;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class AddAccountToPersonRequestDTOTest {
    Denomination denomination;
    Description description;
    PersonID personID;
    AddAccountToPersonRequestDTO addAccountToPersonRequestDTO;
    String denominationA;
    String descriptionA;
    String personEmail;


    @BeforeEach
    void setUp() {
        denomination = new Denomination("School Account");
        description = new Description("Dance School");
        personID = new PersonID(new Email("lia@gmail.com"));

        denominationA = "School Account";
        descriptionA = "Dance School";
        personEmail = "lia@gmail.com";

        addAccountToPersonRequestDTO = AddAccountToPersonRequestAssembler.addAccountMapToDTO(denominationA, descriptionA, personEmail);
    }

    @Test
    void getDenomination() {
        assertEquals(denomination, addAccountToPersonRequestDTO.getDenomination());
    }

    @Test
    void setDenomination() {
        addAccountToPersonRequestDTO.setDenomination(addAccountToPersonRequestDTO.getDenomination());
    }

    @Test
    void getDescription() {
        addAccountToPersonRequestDTO.getDescription();
    }

    @Test
    void setDescription() {
        addAccountToPersonRequestDTO.setDescription(addAccountToPersonRequestDTO.getDescription());
    }

    @Test
    void getPersonID() {
        addAccountToPersonRequestDTO.getPersonID();
    }

    @Test
    void setPersonID() {
        addAccountToPersonRequestDTO.setPersonID(addAccountToPersonRequestDTO.getPersonID());
    }

    @Test
    void testEqualsHappyPath() {
        denomination = new Denomination("School Account");
        description = new Description("Dance School");
        personID = new PersonID(new Email("lia@gmail.com"));

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO1 = new AddAccountToPersonRequestDTO(denomination, description, personID);

        assertEquals(addAccountToPersonRequestDTO, addAccountToPersonRequestDTO1);
    }

    @Test
    void testEqualsHappyPath2() {
        denomination = new Denomination("School Account");
        description = new Description("Dance School");
        personID = new PersonID(new Email("lia@gmail.com"));

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO1 = new AddAccountToPersonRequestDTO(denomination, description, personID);

        assertEquals(addAccountToPersonRequestDTO1, addAccountToPersonRequestDTO1);
    }

    @Test
    void testEqualsDifferentEmail() {
        denomination = new Denomination("School Account");
        description = new Description("Dance School");
        personID = new PersonID(new Email("liar@gmail.com"));

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO1 = new AddAccountToPersonRequestDTO(denomination, description, personID);

        assertFalse(addAccountToPersonRequestDTO.equals(addAccountToPersonRequestDTO1));
    }

    @Test
    void testEqualsDifferentDescription() {
        denomination = new Denomination("School Account");
        description = new Description("Dance Schools");
        personID = new PersonID(new Email("lia@gmail.com"));

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO1 = new AddAccountToPersonRequestDTO(denomination, description, personID);

        assertFalse(addAccountToPersonRequestDTO.equals(addAccountToPersonRequestDTO1));
    }

    @Test
    void testEqualsDifferentDenomination() {
        denomination = new Denomination("Schools Account");
        description = new Description("Dance School");
        personID = new PersonID(new Email("lia@gmail.com"));

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO1 = new AddAccountToPersonRequestDTO(denomination, description, personID);

        assertFalse(addAccountToPersonRequestDTO.equals(addAccountToPersonRequestDTO1));
    }


    @Test
    void testEqualsNullObjects() {

        assertFalse(addAccountToPersonRequestDTO.equals(null));
    }

    @Test()
    void testEqualsDifferentObjects() {

        description = new Description("Dance School");

        assertFalse(addAccountToPersonRequestDTO.equals(description));
    }

    @Test
    void testHashCode() {
        AddAccountToPersonRequestDTO expected = new AddAccountToPersonRequestDTO(denomination, description, personID);

        int result = addAccountToPersonRequestDTO.hashCode();
        int expectedHash = expected.hashCode();

        assertEquals(result, expectedHash);
    }

    @Test
    void testHashCodeHappyPath() {
        AddAccountToPersonRequestDTO expected = new AddAccountToPersonRequestDTO(denomination, description, personID);

        int expectedHash = expected.hashCode();
        int resultHash = -1128349486;

        assertEquals(expectedHash, resultHash);
    }
}