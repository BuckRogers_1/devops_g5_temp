package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.IsSiblingResponseAssembler;

import static org.junit.jupiter.api.Assertions.*;

class IsSiblingResponseDTOTest {
    String isSibling;

    IsSiblingResponseDTO isSiblingResponseDTO;

    @BeforeEach
    void setUp() {
        isSibling = "Hermanos";

        isSiblingResponseDTO = new IsSiblingResponseDTO(isSibling);
    }

    @Test
    void getIsSibling() {
        isSiblingResponseDTO.getIsSibling();
    }

    @Test
    void setIsSibling() {
        isSiblingResponseDTO.setIsSibling(isSiblingResponseDTO.getIsSibling());
    }

    @Test
    void mapToDTOHappyPath() {
        //Arrange
        IsSiblingResponseDTO expected = new IsSiblingResponseDTO(isSibling);

        //Act
        IsSiblingResponseDTO result = IsSiblingResponseAssembler.mapToDTO("Hermanos");

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testEqualsHappyPath() {
        assertEquals(isSiblingResponseDTO.getIsSibling(), isSibling);
    }

    @Test
    void testEqualsNullObjects() {
        assertNotEquals(isSiblingResponseDTO.getIsSibling(), null);
    }

    @Test
    void testEqualsSameObject() {
        // Act
        boolean result = isSiblingResponseDTO.equals(isSiblingResponseDTO);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentObject() {
        // Act
        boolean result = isSiblingResponseDTO.equals(isSibling);

        //Assert
        assertFalse(result);
    }

    @Test
    void testToString() {
        String expected = new IsSiblingResponseDTO(isSibling).toString();
        String result = new IsSiblingResponseDTO("Hermanos").getIsSibling();

        assertEquals(expected, result);
    }

    @Test
    void testHashCode() {
        IsSiblingResponseDTO expected = new IsSiblingResponseDTO(isSibling);
        int result = isSiblingResponseDTO.hashCode();
        int expectedHash = expected.hashCode();

        assertEquals(result, expectedHash);
    }

    @Test
    void testHashCodeExact() {
        int expectedHash = -555730168;

        int result = isSiblingResponseDTO.hashCode();

        assertEquals(result, expectedHash);
    }
}