package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import project.dto.assemblers.CategoryAssembler;
import project.model.shared.Category;
import project.model.shared.Designation;

class CategoryDTOTest {
    Designation designation;
    Category category;
    CategoryDTO categoryDTO;

    @BeforeEach
    void setUp() {
        //Arrange
        designation = new Designation("Groceries");
        category = new Category(designation);
        categoryDTO = new CategoryDTO();

        categoryDTO = CategoryAssembler.mapToDTO(category);

    }

    @Test
    void getDesignation() {
        Designation result = categoryDTO.getDesignation();
    }


}