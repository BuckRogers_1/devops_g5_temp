package project.controllers.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AccountDTO;
import project.dto.AddAccountToPersonRequestDTO;
import project.dto.AddAccountToPersonRequestInfoDTO;
import project.model.account.Account;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.AccountRepository;
import project.repositories.PersonRepository;
import project.services.AddAccountToPersonService;

import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class AddAccountToPersonRestControllerTest {
    PersonID personIdP1;
    Person p1;
    Person p2;
    Email emailP1;
    Email emailP2;
    Person mother;
    Person father;
    LocalDateTime p1BirthDate;
    LocalDateTime fatherBirthDate;
    LocalDateTime motherBirthDate;
    Description description;
    Denomination denomination;
    Description descriptionA;
    Denomination denominationA;
    PersonID personIdP2;

    @Autowired
    private AddAccountToPersonRestController addAccountToPersonRestController;
    @Autowired
    private AddAccountToPersonService addAccountToPersonMockService;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private PersonRepository personRepository;

    @BeforeEach
    void setUp() {
        //Person1
        p1BirthDate = LocalDateTime.of(1982, Month.JANUARY, 01, 0, 0, 0);
        motherBirthDate = LocalDateTime.of(1952, Month.DECEMBER, 15, 0, 0, 0);
        fatherBirthDate = LocalDateTime.of(1951, Month.JANUARY, 10, 0, 0, 0);

        emailP1 = new Email("tarcisio@gmail.com");
        personIdP1 = new PersonID(emailP1);

        mother = new Person(new Name("Maria"), new Address("Porto"), new Date(motherBirthDate), new Email("maria@gmail.com"), null, null);
        father = new Person(new Name("José"), new Address("Porto"), new Date(fatherBirthDate), new Email("jose@gmail.com"), null, null);
        p1 = new Person(new Name("Tarcisio De Jesus"), new Address("Porto"), new Date(p1BirthDate), emailP1, father.getPersonID(), mother.getPersonID());

        personRepository.save(p1);

        denomination = new Denomination("Groceries Account");
        description = new Description("Everything Juicy");
    }

    @DisplayName("addAccountToPerson - HappyPath")
    @Test
    void addAccountToPersonHappyPath() {
        //Arrange
        denominationA = new Denomination("Groceries Account");
        descriptionA = new Description("Everything Juicy");

        emailP2 = new Email("gil@gmail.com");
        personIdP2 = new PersonID(emailP2);
        p2 = new Person(new Name("Gil"), new Address("Porto"), new Date(p1BirthDate), emailP2, father.getPersonID(), mother.getPersonID());
        personRepository.save(p2);

        AccountDTO expected = new AccountDTO();
        expected.setDescription(description);
        expected.setAccountID(new AccountID(denomination, personIdP2));

        final String denominationR = "Groceries Account";
        final String descriptionR = "Everything Juicy";
        final String personEmail = "gil@gmail.com";

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = new AddAccountToPersonRequestDTO(denominationA, descriptionA, personIdP2);
        Mockito.when(addAccountToPersonMockService.addAccountToPerson(addAccountToPersonRequestDTO)).thenReturn(expected);

        //Act
        AddAccountToPersonRequestInfoDTO accountToPersonRequestInfoDTO = new AddAccountToPersonRequestInfoDTO(denominationR, descriptionR);
        ResponseEntity<Object> result = addAccountToPersonRestController.addAccountToPerson("gil@gmail.com", accountToPersonRequestInfoDTO);

        //Assert
        assertEquals(201, result.getStatusCodeValue());
    }

    @DisplayName("addAccountToPerson - Account Already Exist")
    @Test
    void addAccountToPersonAlreadyExist() {
        //Arrange
        Account account = new Account(denomination, description, personIdP1);
        accountRepository.save(account);

        final String denominationR = "Groceries Account";
        final String descriptionR = "Everything Juicy";
        final String personEmail = "tarcisio@gmail.com";

        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = new AddAccountToPersonRequestDTO(denomination, description, personIdP1);
        Mockito.when(addAccountToPersonMockService.addAccountToPerson(addAccountToPersonRequestDTO)).thenThrow(new IllegalArgumentException("Account Already Exist"));

        //Act
        AddAccountToPersonRequestInfoDTO accountToPersonRequestInfoDTO = new AddAccountToPersonRequestInfoDTO(denominationR, descriptionR);

        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            ResponseEntity<Object> result = addAccountToPersonRestController.addAccountToPerson("tarcisio@gmail.com", accountToPersonRequestInfoDTO);
        });
    }
}