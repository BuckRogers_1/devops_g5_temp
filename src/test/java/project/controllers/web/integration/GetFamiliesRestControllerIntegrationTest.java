package project.controllers.web.integration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.AbstractTest;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetFamiliesRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    GroupRepository groupRepository;

    @DisplayName("getFamilies - Happy Path")
    @Test
    @Transactional
    public void getFamiliesHappyPath() throws Exception {
        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));
        Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));

        // Family with two sons
        Name maria = new Name("Maria");
        Email mariaEmail = new Email("mariaconfitada@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);
        Name jose = new Name("José");
        Email joseEmail = new Email("joseabuelas@family.com");
        Person father = new Person(jose, birthAddress, parentsBirthdate, joseEmail, null, null);
        Name tarcisio = new Name("Tarcisio");
        Email tarcisioEmail = new Email("tarcisiogreat@family.com");
        Person son = new Person(tarcisio, birthAddress, personsBirthdate, tarcisioEmail, mother.getPersonID(), father.getPersonID());
        Name nuno = new Name("Nuno");
        Email nunoEmail = new Email("nunohombrero@family.com");
        Person sibling = new Person(nuno, birthAddress, personsBirthdate, nunoEmail, mother.getPersonID(), father.getPersonID());

        personRepository.save(mother);
        personRepository.save(father);
        personRepository.save(son);
        personRepository.save(sibling);

        // GROUPS
        Description familyDescription = new Description("Family of Tarce");
        Group familyGroup = new Group(familyDescription, father.getPersonID());

        familyGroup.addMember(son.getPersonID());
        familyGroup.addMember(mother.getPersonID());
        familyGroup.addMember(sibling.getPersonID());

        groupRepository.save(familyGroup);

        final String uri = "/groups?type=families";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        StringBuilder builder = new StringBuilder(content);
        builder.deleteCharAt(0);
        builder.deleteCharAt(builder.toString().length() - 1);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(builder.toString());
        JsonNode groupNode = rootNode.path("group");
        assertEquals("\"Family of Tarce\"", groupNode.toString());
    }

    @DisplayName("getFamilies - No families found")
    @Test
    @Transactional
    public void getFamiliesNoFamiliesFound() throws Exception {
        //Arrange
        //Families
        Address birthAddress = new Address("Porto, Portugal");
        Date parentsBirthdate = new Date(LocalDateTime.of(1956, 2, 23, 0, 0));

        Name maria = new Name("Maria Rufina");
        Email mariaEmail = new Email("mariaconfitadareis@family.com");
        Person mother = new Person(maria, birthAddress, parentsBirthdate, mariaEmail, null, null);

        personRepository.save(mother);

        // GROUPS
        Description familyDescription = new Description("Maria is alone");
        Group familyGroup = new Group(familyDescription, mother.getPersonID());

        groupRepository.save(familyGroup);

        final String uri = "/groups/?type=families";

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(content);
        JsonNode messageNode = rootNode.path("message");

        assertEquals("", messageNode.asText());
    }
}