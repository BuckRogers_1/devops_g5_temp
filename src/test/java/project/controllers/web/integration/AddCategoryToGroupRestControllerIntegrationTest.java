package project.controllers.web.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import project.AbstractTest;
import project.dto.AddCategoryRequestInfoDTO;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.Address;
import project.model.shared.Date;
import project.model.shared.Email;
import project.model.shared.Name;
import project.repositories.GroupRepository;
import project.repositories.PersonRepository;
import project.utils.GetJsonNodeValue;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AddCategoryToGroupRestControllerIntegrationTest extends AbstractTest {
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    PersonRepository personRepository;

    @DisplayName("addCategoryToGroup - Happy Path")
    @Test
    void addCategoryToGroupHappyPath() throws Exception {
        //Arrange
        final Address birthAddress = new Address("Porto, Portugal");
        final Date personsBirthdate = new Date(LocalDateTime.of(1978, 8, 12, 0, 0));
        final Name rafaelName = new Name("Rafa");
        final Email rafaelEmail = new Email("rafael@family.com");
        final Person rafael = new Person(rafaelName, birthAddress, personsBirthdate, rafaelEmail, null, null);
        final String groupDescription = "CategoryGroupHappy";

        personRepository.save(rafael);
        groupRepository.save(new Group(groupDescription, rafaelEmail.toString()));

        final String uri = "/groups/CategoryGroupHappy/categories";
        final String designation = "GroceriesShop Fresh Account";

        AddCategoryRequestInfoDTO addCategoryRequestInfoDTO = new AddCategoryRequestInfoDTO(designation, groupDescription, rafaelEmail.toString());

        //Act
        String entryJson = super.mapToJson(addCategoryRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("GroceriesShop Fresh Account", GetJsonNodeValue.nodeAsString(content, "designation"));
    }

    @DisplayName("addCategoryToGroup - Person is not responsible")
    @Test
    public void addCategoryToGroupNotResponsible() throws Exception {
        //Arrange
        final String uri = "/groups/CategoryGroupNotResponsible/categories";

        final String designation = "Responsible Groceries Account";
        final String groupDescription = "CategoryGroupNotResponsible";
        final String responsibleEmail = "jose@family.com";
        final String notResponsibleEmail = "mafalda@family.com";

        groupRepository.save(new Group(groupDescription, responsibleEmail));

        AddCategoryRequestInfoDTO addCategoryRequestInfoDTO = new AddCategoryRequestInfoDTO(designation, groupDescription, notResponsibleEmail);
        //Act
        String entryJson = super.mapToJson(addCategoryRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("Please select an existing Responsible", GetJsonNodeValue.nodeAsString(content, "message"));
        assertEquals("UNPROCESSABLE_ENTITY", GetJsonNodeValue.nodeAsString(content, "status"));
    }

    @DisplayName("addCategoryToGroupIDNotFound - GroupID not found")
    @Test
    void addCategoryToGroupIDNotFound() throws Exception {
        //Arrange
        final String uri = "/groups/Inexistant/categories";
        final String designation = "Groceries Fabulous Account";
        final String groupDescription = "Inexistant";
        final String responsibleEmail = "jose@family.com";

        AddCategoryRequestInfoDTO addCategoryRequestInfoDTO = new AddCategoryRequestInfoDTO(designation, groupDescription, responsibleEmail);

        //Act
        String entryJson = super.mapToJson(addCategoryRequestInfoDTO);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(entryJson)).andReturn();

        //Assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(422, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertEquals("Please select an existing GroupID", GetJsonNodeValue.nodeAsString(content, "message"));
        assertEquals("UNPROCESSABLE_ENTITY", GetJsonNodeValue.nodeAsString(content, "status"));
    }
}