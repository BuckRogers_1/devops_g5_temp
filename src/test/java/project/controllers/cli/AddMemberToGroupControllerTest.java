package project.controllers.cli;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddMemberRequestDTO;
import project.dto.GroupDTO;
import project.dto.assemblers.AddMemberRequestAssembler;
import project.exceptions.NotFoundException;
import project.model.group.Group;
import project.model.person.Person;
import project.model.shared.*;
import project.repositories.GroupRepository;
import project.services.AddMemberToGroupService;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * US003:
 * Como gestor de sistema, quero acrescentar pessoas ao grupo.
 */
@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class AddMemberToGroupControllerTest {
    @Autowired
    GroupRepository groupRepository;
    String groupDescription = "Friends";
    String personEmail = "pedro.1234@gmail.com";
    Name p1Name;
    Address p1BirthPlace;
    Date p1BirthDate;
    Email p1Email;
    Person p1;
    PersonID p1ID;
    Name p2Name;
    Address p2BirthPlace;
    Date p2BirthDate;
    Email p2Email;
    Person p2;
    PersonID p2ID;
    Description groupDescription1;
    Group g1;
    GroupID g1ID;
    Description groupDescription2;
    Group g2;
    GroupID g2ID;
    GroupDTO expected;
    Set<PersonID> membersSet;
    Set<Category> categoriesSet;
    @Autowired
    private AddMemberToGroupService addMemberToGroupMockService;
    @Autowired
    private AddMemberToGroupController addMemberToGroupController;

    @BeforeEach
    void setUp() {
        p1Name = new Name("Pedro");
        p1BirthPlace = new Address("Porto");
        p1BirthDate = new Date(LocalDateTime.of(1982, 3, 14, 0, 0));
        p1Email = new Email("pedro.1234@gmail.com");
        p1 = new Person(p1Name, p1BirthPlace, p1BirthDate, p1Email, null, null);
        p1ID = p1.getPersonID();

        p2Name = new Name("Sara");
        p2BirthPlace = new Address("Aveiro");
        p2BirthDate = new Date(LocalDateTime.of(1988, 3, 18, 10, 0));
        p2Email = new Email("sararibeiro88@email.com");
        p2 = new Person(p2Name, p2BirthPlace, p2BirthDate, p2Email, null, null);
        p2ID = p2.getPersonID();

        groupDescription1 = new Description("Friends");
        g1 = new Group(groupDescription1, p1ID);
        g1ID = g1.getID();

        groupDescription2 = new Description("Family2");
        g2 = new Group(groupDescription2, p2ID);
        g2ID = g2.getID();

        groupRepository.save(g1);

        expected = new GroupDTO();
        expected.setGroupID(g1ID);
        membersSet = new HashSet<>();
        categoriesSet = new HashSet<>();
        membersSet.add(p1ID);
        expected.setMembers(membersSet);
        expected.setResponsibles(membersSet);
        expected.setCategories(categoriesSet);
        expected.setCreationDate(new Date(LocalDateTime.now()));
    }

    @DisplayName("addMember - Happy Path")
    @Test
    void addMemberToAGroupHappyPath() {
        //Arrange
        AddMemberRequestDTO addMemberRequestDTO = AddMemberRequestAssembler.mapToDTO(groupDescription, personEmail);
        Mockito.when(addMemberToGroupMockService.addMember(addMemberRequestDTO)).thenReturn(expected);

        //Act
        GroupDTO result = addMemberToGroupController.addMember(g1ID.getDescription().getDescription(), p1ID.getEmail().getEmail());

        //Assert
        assertEquals(expected.getMembers().contains(p1ID), result.getMembers().contains(p1ID));
    }

    @DisplayName("addMember - Exception: Tests if personID is NULL")
    @Test
    void addMemberToAGroupPersonNULL() {
        //Act/Assert
        Exception exception = assertThrows(NullPointerException.class, () -> {
            addMemberToGroupController.addMember(g1ID.getDescription().getDescription(), null);
        });

        String expectedMessage = "E-mail cannot be null";
        String actualMessage = exception.getMessage();

        assertEquals(actualMessage, expectedMessage);
    }

    @DisplayName("addMember - Exception: Tests if groupID is NULL")
    @Test
    void addMemberToAGroupGroupNULL() {
        //Act/Assert
        Exception exception = assertThrows(NullPointerException.class, () -> {
            addMemberToGroupController.addMember(null, p1ID.getEmail().getEmail());
        });
    }

    @DisplayName("addMember - Exception: GroupID not found")
    @Test
    public void addMemberToGroupNotFound() {
        //Arrange
        String groupDescription = "Family123";
        String personEmail = "pedro@gmail.com";

        AddMemberRequestDTO addMemberRequestDTO = AddMemberRequestAssembler.mapToDTO(groupDescription, personEmail);
        Mockito.when(addMemberToGroupMockService.addMember(addMemberRequestDTO)).thenThrow(new NotFoundException("GroupID not found!"));

        //Act
        Exception exception = assertThrows(NotFoundException.class, () -> {
            addMemberToGroupController.addMember("Family123", "pedro@gmail.com");
        });

        String expectedMessage = "GroupID not found!";
        String actualMessage = exception.getMessage();

        //Assert
        assertEquals(expectedMessage, actualMessage);
    }
}