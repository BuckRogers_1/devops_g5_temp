package project.datamodel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class TransactionIDDataTest {
    Email raulEmail;
    Double amount;
    Description description;
    TransactionDate transactionDate;
    Designation designation;

    String category;
    String debitDenomination;
    String creditDenomination;
    String type;

    LedgerData ledgerData;
    LedgerID ledgerID;
    List<TransactionData> transactionsList;
    TransactionData transactionData;

    @BeforeEach
    void setUp() {
        raulEmail = new Email("raul@family.com");

        //Transaction
        amount = 25.0;
        description = new Description("Gym");
        transactionDate = new TransactionDate(LocalDateTime.of(12, Month.MAY, 03, 12, 12));
        designation = new Designation("MonthlyPayment");
        category = "MonthlyPayment";
        debitDenomination = "Wallet";
        creditDenomination = "Bank";
        type = "DEBIT";

        ledgerID = new LedgerID(new PersonID(raulEmail));
        ledgerData = new LedgerData(ledgerID);

        transactionsList = new ArrayList<>();
        transactionData = new TransactionData(amount, description, transactionDate, category, debitDenomination, creditDenomination, type, ledgerData);
        transactionsList.add(transactionData);
    }

    @Test
    void getId() {
        //Arrange
        TransactionIDData transactionIDData = new TransactionIDData();
        transactionIDData.setLedger(ledgerData);
        transactionIDData.setId("352");

        String stringExpected = "352";

        //Act
        String transactionIDDataToString = transactionIDData.getId();

        //Assert
        assertEquals(stringExpected, transactionIDDataToString);
    }

    @Test
    void getLedger() {
        //Arrange
        TransactionIDData transactionIDData = new TransactionIDData();
        transactionIDData.setLedger(ledgerData);
        transactionIDData.setId("352");

        //Act
        LedgerData ledgerResult = transactionIDData.getLedger();

        //Assert
        assertEquals(ledgerData, ledgerResult);
    }

    @DisplayName("Same Object")
    @Test
    void testEqualsSameObject() {
        //Arrange
        TransactionIDData transactionIDData = new TransactionIDData();
        transactionIDData.setLedger(ledgerData);
        transactionIDData.setId("352");

        //Act
        LedgerData ledgerResult = transactionIDData.getLedger();

        //Assert
        assertEquals(transactionIDData, transactionIDData);
    }

    @DisplayName("Diff Objects")
    @Test
    void testEqualsDiffObjects() {
        //Arrange
        TransactionIDData transactionIDData = new TransactionIDData();
        transactionIDData.setLedger(ledgerData);
        transactionIDData.setId("352");

        //Act
        LedgerData ledgerResult = transactionIDData.getLedger();

        //Assert
        assertNotEquals(transactionIDData, ledgerData);
    }

    @Test
    void testHashCode() {
        //Arrange
        TransactionIDData transactionIDDataHash = new TransactionIDData();
        int hashCodeExpected = 31;

        //Act
        int hashCodeResult = transactionIDDataHash.hashCode();

        //Assert
        assertEquals(hashCodeExpected, hashCodeResult);
    }

    @Test
    void testToString() {
        //Arrange
        TransactionIDData transactionIDData = new TransactionIDData();
        transactionIDData.setLedger(ledgerData);
        transactionIDData.setId("332");

        String stringExpected = "332";

        //Act
        String transactionIDDataToString = transactionIDData.toString();

        //Assert
        assertEquals(stringExpected, transactionIDDataToString);
    }
}