package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.shared.Description;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class MemberIDDataTest {
    @Test
    void getId() {
        //Arrange
        MemberIDData memberIDData = new MemberIDData("John Doe");

        String expected = "John Doe";

        //Act
        String result = memberIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setId() {
        //Arrange
        MemberIDData memberIDData = new MemberIDData("John Doe");

        String expected = "John Doe 2";

        //Act
        memberIDData.setId("John Doe 2");
        String result = memberIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testEquals() {
        //Arrange
        MemberIDData memberIDData = new MemberIDData("John Doe");

        //Act
        boolean result = memberIDData.equals(memberIDData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentObjectSameInfo() {
        //Arrange
        MemberIDData memberIDData = new MemberIDData("John Doe");
        MemberIDData memberIDDataSameInfo = new MemberIDData("John Doe");

        //Act
        //Assert
        assertEquals(memberIDDataSameInfo, memberIDData);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        MemberIDData memberIDData = new MemberIDData("John Doe");
        MemberIDData memberIDDataSameInfo = null;

        //Act
        boolean result = memberIDData.equals(memberIDDataSameInfo);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObject() {
        //Arrange
        MemberIDData memberIDData = new MemberIDData("John Doe");
        Description description = new Description("Description");

        //Act
        boolean result = memberIDData.equals(description);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        MemberIDData memberIDData = new MemberIDData("John Doe");

        int expected = Objects.hash(memberIDData.getId());

        //Act
        int result = memberIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeFixedValue() {
        //Arrange
        MemberIDData memberIDData = new MemberIDData("John Doe");

        int expected = -1367319356;

        //Act
        int result = memberIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        MemberIDData memberIDData = new MemberIDData("John Doe");

        String expected = "MemberIDData(id=John Doe)";

        //Act
        String result = memberIDData.toString();

        //Assert
        assertEquals(expected, result);
    }
}