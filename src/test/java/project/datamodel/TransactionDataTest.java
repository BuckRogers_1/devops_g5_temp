package project.datamodel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TransactionDataTest {
    Email tarcisioEmail;
    Double amount;
    Description description;
    TransactionDate transactionDate;
    Designation designation;

    String category;
    String debitDenomination;
    String creditDenomination;
    String type;

    LedgerData ledgerDataExpected;
    LedgerID ledgerID;
    List<TransactionData> transactionsList;
    TransactionData transactionData;

    @BeforeEach
    void setUp() {
        tarcisioEmail = new Email("tarcisio@family.com");

        //Transaction
        amount = 25.0;
        description = new Description("Gym");
        transactionDate = new TransactionDate(LocalDateTime.of(12, Month.MAY, 03, 12, 12));
        designation = new Designation("MonthlyPayment");
        category = "MonthlyPayment";
        debitDenomination = "Wallet";
        creditDenomination = "Bank";
        type = "DEBIT";

        ledgerID = new LedgerID(new PersonID(tarcisioEmail));
        ledgerDataExpected = new LedgerData(ledgerID);

        transactionsList = new ArrayList<>();
        transactionData = new TransactionData(amount, description, transactionDate, category, debitDenomination, creditDenomination, type, ledgerDataExpected);
        transactionsList.add(transactionData);
    }

    @Test
    void getTransactionId() {
        //Arrange
        TransactionIDData transactionIDDataExpected = new TransactionIDData("0012-05-03T12:12", ledgerDataExpected);

        //Act
        TransactionIDData transactionIDData = transactionData.getTransactionId();

        //Assert
        assertTrue(transactionIDData.equals(transactionIDDataExpected));
    }

    @Test
    void setTransactionId() {
        //Arrange
        TransactionIDData transactionIDDataExpected = new TransactionIDData("0012-05-33T15:15", ledgerDataExpected);

        //Act
        transactionData.setTransactionId(new TransactionIDData("0012-05-33T15:15", ledgerDataExpected));
        TransactionIDData transactionIDData = transactionData.getTransactionId();

        //Assert
        assertTrue(transactionIDData.equals(transactionIDDataExpected));
    }

    @Test
    void getAmount() {
        //Arrange
        Double amountExpected = 25.0;

        //Act
        Double amountResult = transactionData.getAmount();

        //Assert
        assertTrue(amountResult.equals(amountExpected));
    }

    @Test
    void setAmount() {
        //Arrange
        Double amountExpected = 50.5;

        //Act
        transactionData.setAmount(50.5);
        Double amountResult = transactionData.getAmount();

        //Assert
        assertTrue(amountResult.equals(amountExpected));
    }

    @Test
    void getDescription() {
        //Arrange
        Description descriptionExpected = new Description("Gym");

        //Act
        Description descriptionResult = transactionData.getDescription();

        //Assert
        assertTrue(descriptionResult.equals(descriptionExpected));
    }

    @Test
    void setDescription() {
        //Arrange
        Description descriptionExpected = new Description("Yoga");

        //Act
        transactionData.setDescription(new Description("Yoga"));
        Description descriptionResult = transactionData.getDescription();

        //Assert
        assertTrue(descriptionResult.equals(descriptionExpected));
    }

    @Test
    void getCategory() {
        //Arrange
        String categoryExpected = "MonthlyPayment";

        //Act
        String categoryResult = transactionData.getCategory();

        //Assert
        assertTrue(categoryResult.equals(categoryExpected));
    }

    @Test
    void setCategory() {
        //Arrange
        String categoryExpected = "WeekPayment";

        //Act
        transactionData.setCategory("WeekPayment");
        String categoryResult = transactionData.getCategory();

        //Assert
        assertTrue(categoryResult.equals(categoryExpected));
    }

    @Test
    void getDebitDenomination() {
        //Arrange
        String debitDenominationExpected = "Wallet";

        //Act
        String debitDenominationResult = transactionData.getDebitDenomination();

        //Assert
        assertTrue(debitDenominationResult.equals(debitDenominationExpected));
    }

    @Test
    void setDebitDenomination() {
        //Arrange
        String debitDenominationExpected = "MyWallet";

        //Act
        transactionData.setDebitDenomination("MyWallet");
        String debitDenominationResult = transactionData.getDebitDenomination();

        //Assert
        assertTrue(debitDenominationResult.equals(debitDenominationExpected));
    }

    @Test
    void getCreditDenomination() {
        //Arrange
        String creditDenominationExpected = "Bank";

        //Act
        String creditDenominationResult = transactionData.getCreditDenomination();

        //Assert
        assertTrue(creditDenominationResult.equals(creditDenominationExpected));
    }

    @Test
    void setCreditDenomination() {
        //Arrange
        String creditDenominationExpected = "MyBank";

        //Act
        transactionData.setCreditDenomination("MyBank");
        String creditDenominationResult = transactionData.getCreditDenomination();

        //Assert
        assertTrue(creditDenominationResult.equals(creditDenominationExpected));
    }

    @Test
    void getType() {
        //Arrange
        String typeExpected = "DEBIT";

        //Act
        String typeResult = transactionData.getType();

        //Assert
        assertTrue(typeResult.equals(typeExpected));
    }

    @Test
    void setType() {
        //Arrange
        String typeExpected = "CREDIT";

        //Act
        transactionData.setType("CREDIT");
        String typeResult = transactionData.getType();

        //Assert
        assertTrue(typeResult.equals(typeExpected));
    }

    @DisplayName("Equals - Same Object")
    @Test
    void testEqualsSameObject() {
        //Assert
        assertTrue(transactionData.equals(transactionData));
    }

    @DisplayName("Equals - Same content but two objects")
    @Test
    void testEqualsSameContent() {
        //Assert//Act
        TransactionData transactionDataExpected = new TransactionData(amount, description, transactionDate, category, debitDenomination, creditDenomination, type, ledgerDataExpected);
        //Assert
        assertTrue(transactionData.equals(transactionDataExpected));
    }

    @DisplayName("Equals - Null Object")
    @Test
    void testEqualsNull() {
        //Assert//Act
        TransactionData transactionDataExpected = null;
        //Assert
        assertFalse(transactionData.equals(transactionDataExpected));
    }

    @DisplayName("Equals - Different Objects")
    @Test
    void testEqualsDiffObjects() {
        //Assert
        assertFalse(transactionData.equals(tarcisioEmail));
    }

    @DisplayName("Equals - Different TransactionId")
    @Test
    void testEqualsDiffTransactionId() {
        TransactionData transactionDataExpected = new TransactionData(amount, description, transactionDate, category, debitDenomination, creditDenomination, type, ledgerDataExpected);
        transactionDataExpected.setTransactionId(new TransactionIDData("0012-05-33T15:15", ledgerDataExpected));

        //Assert
        assertFalse(transactionData.equals(transactionDataExpected));
    }

    @DisplayName("Equals - Different Amount")
    @Test
    void testEqualsDiffAmount() {
        TransactionData transactionDataExpected = new TransactionData(amount, description, transactionDate, category, debitDenomination, creditDenomination, type, ledgerDataExpected);
        transactionDataExpected.setAmount(50.50);

        //Assert
        assertFalse(transactionData.equals(transactionDataExpected));
    }

    @DisplayName("Equals - Different Description")
    @Test
    void testEqualsDiffDescription() {
        TransactionData transactionDataExpected = new TransactionData(amount, description, transactionDate, category, debitDenomination, creditDenomination, type, ledgerDataExpected);
        transactionDataExpected.setDescription(new Description("SurfLessons"));

        //Assert
        assertFalse(transactionData.equals(transactionDataExpected));
    }

    @DisplayName("Equals - Different Category")
    @Test
    void testEqualsDiffCategory() {
        TransactionData transactionDataExpected = new TransactionData(amount, description, transactionDate, category, debitDenomination, creditDenomination, type, ledgerDataExpected);
        transactionDataExpected.setCategory("Hobbies");

        //Assert
        assertFalse(transactionData.equals(transactionDataExpected));
    }

    @DisplayName("Equals - Different DebitDenomination")
    @Test
    void testEqualsDiffDebitDenomination() {
        TransactionData transactionDataExpected = new TransactionData(amount, description, transactionDate, category, debitDenomination, creditDenomination, type, ledgerDataExpected);
        transactionDataExpected.setDebitDenomination("Pocket");

        //Assert
        assertFalse(transactionData.equals(transactionDataExpected));
    }

    @DisplayName("Equals - Different CreditDenomination")
    @Test
    void testEqualsDiffCreditDenomination() {
        TransactionData transactionDataExpected = new TransactionData(amount, description, transactionDate, category, debitDenomination, creditDenomination, type, ledgerDataExpected);
        transactionDataExpected.setCreditDenomination("MyPocket");

        //Assert
        assertFalse(transactionData.equals(transactionDataExpected));
    }

    @DisplayName("Equals - Different Type")
    @Test
    void testEqualsDiffType() {
        TransactionData transactionDataExpected = new TransactionData(amount, description, transactionDate, category, debitDenomination, creditDenomination, type, ledgerDataExpected);
        transactionDataExpected.setType("CREDIT");

        //Assert
        assertFalse(transactionData.equals(transactionDataExpected));
    }

    @DisplayName("HashCode")
    @Test
    void hashCodeSame() {
        //Arrange
        TransactionData transactionDataHash = new TransactionData();
        int hashCodeExpected = 1742810335;

        // Act
        int hashCodeResult = transactionDataHash.hashCode();

        //Assert
        assertEquals(hashCodeExpected, hashCodeResult);
    }
}