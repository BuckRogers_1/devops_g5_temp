package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class SiblingIDDataTest {
    @Test
    void getId() {
        //Arrange
        SiblingIDData noArgsSiblingIDData = new SiblingIDData();
        SiblingIDData personIdArgsPerson = new SiblingIDData(new PersonID(new Email("jimbocas@gmail.com")));
        SiblingIDData siblingIDData = new SiblingIDData("John Doe");

        String expected = "John Doe";

        //Act
        String result = siblingIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setId() {
        //Arrange
        SiblingIDData siblingIDData = new SiblingIDData("John Doe");

        String expected = "John Doe 2";

        //Act
        siblingIDData.setId("John Doe 2");
        String result = siblingIDData.getId();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testEquals() {
        //Arrange
        SiblingIDData siblingIDData = new SiblingIDData("John Doe");

        //Act
        boolean result = siblingIDData.equals(siblingIDData);

        //Assert
        assertTrue(result);
    }

    @Test
    void testEqualsDifferentObjectSameInfo() {
        //Arrange
        SiblingIDData siblingIDData = new SiblingIDData("John Doe");
        SiblingIDData siblingIDDataSameInfo = new SiblingIDData("John Doe");

        //Act
        //Assert
        assertEquals(siblingIDDataSameInfo, siblingIDData);
    }

    @Test
    void testEqualsNullObject() {
        //Arrange
        SiblingIDData siblingIDData = new SiblingIDData("John Doe");
        SiblingIDData siblingIDDataSameInfo = null;

        //Act
        boolean result = siblingIDData.equals(siblingIDDataSameInfo);

        //Assert
        assertFalse(result);
    }

    @Test
    void testEqualsDifferentObject() {
        //Arrange
        SiblingIDData siblingIDData = new SiblingIDData("John Doe");
        Description description = new Description("Description");

        //Act
        boolean result = siblingIDData.equals(description);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        SiblingIDData siblingIDData = new SiblingIDData("John Doe");

        int expected = Objects.hash(siblingIDData.getId());

        //Act
        int result = siblingIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testHashCodeFixedValue() {
        //Arrange
        SiblingIDData siblingIDData = new SiblingIDData("John Doe");

        int expected = -1367319356;

        //Act
        int result = siblingIDData.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        SiblingIDData siblingIDData = new SiblingIDData("John Doe");

        String expected = "SiblingIDData(id=John Doe)";

        //Act
        String result = siblingIDData.toString();

        //Assert
        assertEquals(expected, result);
    }
}