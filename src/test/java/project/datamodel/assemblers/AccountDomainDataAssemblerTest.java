package project.datamodel.assemblers;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.datamodel.AccountData;
import project.model.account.Account;
import project.model.shared.AccountID;
import project.model.shared.Denomination;
import project.model.shared.Email;
import project.model.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AccountDomainDataAssemblerTest {
    @DisplayName("Convert an Account object to AccountData object")
    @Test
    void toData() {
        //Arrange
        Denomination denomination = new Denomination("Account Denomination");
        PersonID ownerID = new PersonID(new Email("barrotes@gmail.com"));
        AccountID accountID = new AccountID(denomination, ownerID);
        Account domainAccount = new Account(accountID, "Account Description");

        AccountData expected = new AccountData(accountID, "Account Description");

        //Act
        AccountData result = AccountDomainDataAssembler.toData(domainAccount);

        //Assert
        assertEquals(expected, result);
    }

    @DisplayName("Convert an AccountData object to Account object")
    @Test
    void toDomain() {
        //Arrange
        Denomination denomination = new Denomination("Account Denomination");
        PersonID ownerID = new PersonID(new Email("barrotes@gmail.com"));
        AccountID accountID = new AccountID(denomination, ownerID);
        AccountData accountData = new AccountData(accountID, "Account Description");

        Account expected = new Account(accountID, "Account Description");

        //Act
        Account result = AccountDomainDataAssembler.toDomain(accountData);

        //Assert
        assertEquals(expected, result);
    }
}