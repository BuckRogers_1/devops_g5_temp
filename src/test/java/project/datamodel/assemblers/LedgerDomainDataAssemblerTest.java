package project.datamodel.assemblers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.datamodel.LedgerData;
import project.datamodel.TransactionData;
import project.model.ledger.Ledger;
import project.model.ledger.Transaction;
import project.model.person.Person;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LedgerDomainDataAssemblerTest {
    Address birthAddress;
    Date personsBirthdate;
    Name rodrigo;
    Email rodrigoEmail;
    Person p1;
    Double amount;
    Description description;
    TransactionDate transactionDate;
    Designation designation;
    Category category;
    Denomination denominationDebit;
    AccountID debit;
    Denomination denominationCredit;
    AccountID credit;
    TransactionType type;

    @BeforeEach
    void setUp() {
        //Person
        birthAddress = new Address("Guimarães, Portugal");
        personsBirthdate = new Date(LocalDateTime.of(1987, 8, 12, 0, 0));
        rodrigo = new Name("Rodrigo");
        rodrigoEmail = new Email("rodrigo@family.com");
        p1 = new Person(rodrigo, birthAddress, personsBirthdate, rodrigoEmail, null, null);

        //Transaction
        amount = 25.0;
        description = new Description("Gym");
        transactionDate = new TransactionDate(LocalDateTime.of(12, Month.MAY, 03, 12, 12));
        designation = new Designation("MonthlyPayment");
        category = new Category(designation);
        denominationDebit = new Denomination("Wallet");
        debit = new AccountID(denominationDebit, p1.getPersonID());
        denominationCredit = new Denomination("Bank");
        credit = new AccountID(denominationCredit, p1.getPersonID());
        type = TransactionType.DEBIT;
    }

    @DisplayName("LedgerDomainDataAssembler - toData")
    @Test
    void toData() {
        //Ledger
        Ledger ledger = new Ledger(new LedgerID(new PersonID(rodrigoEmail)));
        Transaction transaction = new Transaction(amount, description, transactionDate, category, debit, credit, type);
        ledger.addTransaction(transaction);

        //LedgerData
        String category1 = "MonthlyPayment";
        String debitDenomination = "Wallet";
        String creditDenomination = "Bank";
        String type1 = "DEBIT";

        LedgerID ledgerID = new LedgerID(p1.getPersonID());
        LedgerData ledgerDataExpected = new LedgerData(ledgerID);

        List<TransactionData> transactionsList = new ArrayList<>();
        TransactionData transactionData = new TransactionData(amount, description, transactionDate, category1, debitDenomination, creditDenomination, type1, ledgerDataExpected);
        transactionsList.add(transactionData);

        ledgerDataExpected.setTransactions(transactionsList);

        //Act
        LedgerData ledgerDataResult = LedgerDomainDataAssembler.toData(ledger);

        //Assert
        assertEquals(ledgerDataExpected.getLedgerID(), ledgerDataResult.getLedgerID());
    }

    @DisplayName("LedgerDomainDataAssembler - toDomain")
    @Test
    void toDomain() {
        //Ledger
        Ledger ledger = new Ledger(new LedgerID(new PersonID(rodrigoEmail)));
        Transaction transaction = new Transaction(amount, description, transactionDate, category, debit, credit, type);
        ledger.addTransaction(transaction);

        //LedgerData
        String category1 = "MonthlyPayment";
        String debitDenomination = "Wallet";
        String creditDenomination = "Bank";
        String type1 = "DEBIT";

        LedgerID ledgerID = new LedgerID(p1.getPersonID());

        LedgerData ledgerDataExpected = new LedgerData(ledgerID);

        List<TransactionData> transactionsList = new ArrayList<>();
        TransactionData transactionData = new TransactionData(amount, description, transactionDate, category1, debitDenomination, creditDenomination, type1, ledgerDataExpected);
        transactionsList.add(transactionData);

        ledgerDataExpected.setTransactions(transactionsList);

        //Act
        Ledger ledgerResult = LedgerDomainDataAssembler.toDomain(ledgerDataExpected);

        //Assert
        assertEquals(ledger.getLedgerID(), ledgerResult.getLedgerID());
    }
}