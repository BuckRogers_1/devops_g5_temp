package project.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AccountDTO;
import project.dto.AddAccountToPersonRequestDTO;
import project.dto.assemblers.AccountAssembler;
import project.exceptions.AlreadyExistsException;
import project.exceptions.NotFoundException;
import project.model.account.Account;
import project.model.shared.AccountID;
import project.repositories.AccountRepository;
import project.repositories.PersonRepository;

@Service
public class AddAccountToPersonService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    PersonRepository personRepository;


    /**
     * Add an account, attributing a denomination and description, to later be used in my movements
     *
     * @param addAccountToPersonRequestDTO With information from web to create account
     * @return DTO with the information of the added account in repository
     */
    public AccountDTO addAccountToPerson(AddAccountToPersonRequestDTO addAccountToPersonRequestDTO) {

        if (!personRepository.findById(addAccountToPersonRequestDTO.getPersonID()).isPresent()) {
            throw new NotFoundException("Person");
        }

        if (accountRepository.findById(new AccountID(addAccountToPersonRequestDTO.getDenomination(), addAccountToPersonRequestDTO.getPersonID())).isPresent()) {
            throw new AlreadyExistsException("Account");
        }

        Account account = new Account(addAccountToPersonRequestDTO.getDenomination(), addAccountToPersonRequestDTO.getDescription(), addAccountToPersonRequestDTO.getPersonID());
        accountRepository.save(account);

        return AccountAssembler.mapToDTO(account);
    }

}
