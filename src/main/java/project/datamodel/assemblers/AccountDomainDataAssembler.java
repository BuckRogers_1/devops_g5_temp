package project.datamodel.assemblers;

import org.springframework.stereotype.Service;
import project.datamodel.AccountData;
import project.model.account.Account;

/**
 * Conversion Class for Account Objects used in database operations
 */
@Service
public class AccountDomainDataAssembler {
    /**
     * Converts a received Domain Account Object to Database Data Object, to store in DB
     *
     * @param account
     * @return instance of AccountData
     */
    public static AccountData toData(Account account) {
        return new AccountData(account.getAccountID(), account.getDescription().toString());
    }

    /**
     * Converts a received Database Data Object to Domain Account Object for use in application
     *
     * @param accountData
     * @return instance of Domain Account Object
     */
    public static Account toDomain(AccountData accountData) {
        return new Account(accountData.getAccountID(), accountData.getDescription());
    }
}