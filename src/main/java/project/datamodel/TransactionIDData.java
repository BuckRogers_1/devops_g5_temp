package project.datamodel;

import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor

@Embeddable
public class TransactionIDData implements Serializable {
    private static final long serialVersionUID = 9198900360351045241L;
    private String id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ledger_id", nullable = false)
    private LedgerData ledger;

    public TransactionIDData(String id, LedgerData ledger) {
        this.id = id;
        this.ledger = ledger;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LedgerData getLedger() {
        return ledger;
    }

    public void setLedger(LedgerData ledger) {
        this.ledger = ledger;
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TransactionIDData that = (TransactionIDData) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
