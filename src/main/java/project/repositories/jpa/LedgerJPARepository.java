package project.repositories.jpa;

import org.springframework.data.repository.CrudRepository;
import project.datamodel.LedgerData;
import project.model.shared.LedgerID;

public interface LedgerJPARepository extends CrudRepository<LedgerData, LedgerID> {
}
