package project.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.datamodel.AccountData;
import project.datamodel.assemblers.AccountDomainDataAssembler;
import project.model.account.Account;
import project.model.framework.Repository;
import project.model.shared.AccountID;
import project.model.shared.Denomination;
import project.model.shared.OwnerID;
import project.repositories.jpa.AccountJPARepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class AccountRepository implements Repository {
    @Autowired
    private AccountJPARepository accountJPARepository;

    /**
     * Saves an entity either by creating it or updating it in the persistence
     * store. The reference is no
     * longer valid and use the returned object instead. e.g.,
     *
     * @param entity
     * @return the object reference to the persisted object.
     */
    @Transactional
    public Account save(Object entity) {
        Account newAccount = (Account) entity;
        System.out.println("AccountRepository Entry Account > " + newAccount.getAccountID().toString());
        AccountData accountData = AccountDomainDataAssembler.toData(newAccount);
        AccountData savedAccount = accountJPARepository.save(accountData);
        System.out.println("AccountRepository JPA Return > " + savedAccount.getAccountID().toString());
        return AccountDomainDataAssembler.toDomain(savedAccount);
    }

    /**
     * Gets all entities from the repository.
     *
     * @return all entities from the repository
     */
    public Iterable<Account> findAll() {
        Iterable<AccountData> allAccountData = accountJPARepository.findAll();
        List<Account> domainAccounts = new ArrayList<>();
        for (AccountData accountData : allAccountData) {
            domainAccounts.add(AccountDomainDataAssembler.toDomain(accountData));
        }
        return domainAccounts;
    }

    /**
     * Gets the entity with the specified primary key.
     *
     * @param id
     * @return the entity with the specified primary key
     */
    public Optional<Account> findById(Object id) {
        AccountID accountIdToSearch = (AccountID) id;
        Optional<Account> foundAccount = Optional.empty();

        if (accountJPARepository.findById(accountIdToSearch).isPresent()) {
            AccountData accountData = accountJPARepository.findById(accountIdToSearch).get();
            foundAccount = Optional.of(AccountDomainDataAssembler.toDomain(accountData));
        }

        return foundAccount;
    }

    public void deleteAll() {
        accountJPARepository.deleteAll();
    }

    public Optional<Account> findByAccountID_DenominationAndAccountID_OwnerID(Denomination denomination, OwnerID ownerID) {
        AccountData savedAccount = accountJPARepository.findByAccountID_DenominationAndAccountID_OwnerID(denomination, ownerID);

        return Optional.of(AccountDomainDataAssembler.toDomain(savedAccount));
    }

    public Iterable<Account> findAllByAccountID_OwnerID(OwnerID ownerId) {
        Iterable<AccountData> allAccountData = accountJPARepository.findAllByAccountID_OwnerID(ownerId);
        List<Account> domainAccounts = new ArrayList<>();
        for (AccountData accountData : allAccountData) {
            domainAccounts.add(AccountDomainDataAssembler.toDomain(accountData));
        }
        return domainAccounts;
    }
}
