package project.model.shared;

import lombok.NoArgsConstructor;
import project.exceptions.EmptyException;
import project.model.framework.ValueObject;

import javax.persistence.Embeddable;
import java.util.Objects;

@NoArgsConstructor

@Embeddable
public class Denomination implements ValueObject<Denomination> {
    private String denomination;

    public Denomination(String denomination) {
        setIsDenominationValid(denomination);
    }

    private void setIsDenominationValid(String denomination) {
        if (denomination == null) {
            throw new NullPointerException();
        }
        if (denomination.trim().isEmpty()) {
            throw new EmptyException();
        }
        this.denomination = denomination.trim();
    }

    public String getDenomination() {
        return denomination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Denomination that = (Denomination) o;
        return denomination.equals(that.denomination);
    }

    @Override
    public String toString() {
        return denomination;
    }

    @Override
    public int hashCode() {
        return Objects.hash(denomination);
    }

    @Override
    public boolean sameValueAs(Denomination other) {
        return other != null && this.denomination.equals(other.denomination);
    }
}
