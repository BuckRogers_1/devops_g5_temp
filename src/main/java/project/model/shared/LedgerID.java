package project.model.shared;

import lombok.NoArgsConstructor;
import lombok.ToString;
import project.model.framework.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@ToString
@NoArgsConstructor

@Embeddable
public class LedgerID implements ValueObject<LedgerID>, Serializable {
    private static final long serialVersionUID = -798349380411466L;
    OwnerID id;

    public LedgerID(OwnerID ownerID) {
        this.id = ownerID;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public OwnerID getId() {
        return id;
    }

    public void setId(OwnerID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LedgerID ledgerID = (LedgerID) o;
        return Objects.equals(id, ledgerID.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean sameValueAs(LedgerID other) {
        return false;
    }

}