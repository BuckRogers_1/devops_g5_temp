package project.model.shared;

public enum TransactionType {
    CREDIT,
    DEBIT
}