package project.model.shared;

import project.model.framework.ValueObject;

import java.util.Objects;

public class Address implements ValueObject<Address> {
    private String address;

    public Address(String address) {
        setIsAddressValid(address);
    }

    private void setIsAddressValid(String address) {
        if (address == null) {
            throw new NullPointerException("Name cannot be null");
        }
        if (address.trim().isEmpty()) {
            throw new IllegalArgumentException("Name is invalid");
        }

        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Address address1 = (Address) o;
        return address.equals(address1.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address);
    }

    @Override
    public boolean sameValueAs(Address other) {
        return other != null && this.address.equals(other.address);
    }

    @Override
    public String toString() {
        return address;
    }
}

