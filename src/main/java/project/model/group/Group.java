package project.model.group;

import project.model.framework.Root;
import project.model.shared.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public final class Group implements Root<Group> {
    private final GroupID groupID;
    private final Set<PersonID> responsibles;
    private final Set<PersonID> members;
    private final Set<Category> categories;
    private Date creationDate;

    public Group(Description description, PersonID responsible) {
        validateInput(responsible);
        this.groupID = new GroupID(description);
        this.creationDate = new Date(LocalDateTime.now());
        this.responsibles = new HashSet<>();
        this.responsibles.add(responsible);
        this.members = new HashSet<>();
        this.members.add(responsible);
        this.categories = new HashSet<>();
    }

    public Group(String description, String responsibleEmail) {
        PersonID responsibleID = new PersonID(new Email(responsibleEmail));
        this.groupID = new GroupID(new Description(description));
        this.creationDate = new Date(LocalDateTime.now());
        this.responsibles = new HashSet<>();
        this.responsibles.add(responsibleID);
        this.members = new HashSet<>();
        this.members.add(responsibleID);
        this.categories = new HashSet<>();
    }

    public Set<PersonID> getResponsibles() {
        return new HashSet<>(responsibles);
    }

    public Set<Category> getCategories() {
        return new HashSet<>(categories);
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public GroupID getID() {
        return groupID;
    }

    public boolean isResponsible(PersonID personID) {
        return this.responsibles.contains(personID);
    }

    /**
     * Add a category to the list of categories of the group
     *
     * @param category
     * @return boolean
     */
    public boolean addCategory(Category category) {
        return this.categories.add(category);
    }

    /**
     * Check's if a determined personID is contained in a given group's set of membersID's.
     *
     * @param personID
     * @return boolean mID
     */
    public boolean isMemberID(PersonID personID) {
        boolean mID = false;
        if (this.members.contains(personID)) {
            mID = true;
        }
        return mID;
    }

    /**
     * Adds given personID to members set
     *
     * @param personID
     * @return
     */
    public PersonID addMember(PersonID personID) {
        validateInput(personID);
        this.members.add(personID);
        return personID;
    }

    /**
     * Add a responsible to a group
     *
     * @param personID
     * @return personID of the responsible
     */
    public PersonID addResponsible(PersonID personID) {
        validateInput(personID);
        this.responsibles.add(personID);
        return personID;
    }

    /**
     * Get's all group membersID's.
     *
     * @return Set<PersonID> members
     */
    public Set<PersonID> getMembers() {
        return new HashSet<>(members);
    }

    /**
     * Validates PersonID input
     *
     * @param personID
     */
    private void validateInput(PersonID personID) {
        if (personID == null) {
            throw new NullPointerException("personID cannot be null");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group group = (Group) o;
        return Objects.equals(groupID, group.groupID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupID, responsibles);
    }

    /**
     * Roots compare by identity
     *
     * @param other
     * @return
     */
    @Override
    public boolean sameIdentityAs(Group other) {
        return this.groupID.sameValueAs(other.groupID);
    }

    @Override
    public String toString() {
        StringBuilder allResponsibles = new StringBuilder();

        for (PersonID responsible : responsibles) {
            allResponsibles.append(responsible.getEmail().toString());
        }

        StringBuilder allMembers = new StringBuilder();

        for (PersonID member : members) {
            allMembers.append(member.getEmail().toString()).append(",");
        }

        return "Group{" +
                "groupID=" + groupID.getDescription().toString() +
                ", responsibles=" + allResponsibles +
                ", members=" + allMembers +
                ", categories=" + categories +
                ", creationDate=" + creationDate +
                '}';
    }
}