package project.model.account;

import project.model.framework.Root;
import project.model.shared.AccountID;
import project.model.shared.Denomination;
import project.model.shared.Description;
import project.model.shared.OwnerID;

import java.util.Objects;

public final class Account implements Root<Account> {
    private final Description description;
    private final AccountID accountID;

    /**
     * Account Domain Object creation
     *
     * @param denomination
     * @param description
     * @param ownerID
     */
    public Account(Denomination denomination, Description description, OwnerID ownerID) {
        this.accountID = new AccountID(denomination, ownerID);
        this.description = description;
    }

    public Account(AccountID accountID, String description) {
        this.accountID = accountID;
        this.description = new Description(description);
    }

    public AccountID getAccountID() {
        return accountID;
    }

    public Description getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return Objects.equals(description, account.description) &&
                Objects.equals(accountID, account.accountID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, accountID);
    }

    @Override
    public String toString() {
        return "Account{" +
                "description=" + description +
                ", accountID=" + accountID +
                '}';
    }

    public boolean sameIdentityAs(final Account other) {
        return other != null && this.accountID.equals(other.accountID);
    }
}
