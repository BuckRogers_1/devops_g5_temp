package project.model.person;

import project.model.framework.Root;
import project.model.shared.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Person implements Root<Person> {
    private final PersonID personID;
    private final Name name;
    private final Address birthPlace;
    private final Date birthDate;
    private final PersonID mother;
    private final PersonID father;
    private final Set<PersonID> siblings;
    private final Set<Category> categories;

    /**
     * Constructor Person
     *
     * @param name
     * @param birthPlace
     * @param birthDate
     * @param email
     * @param mother
     * @param father
     */
    public Person(Name name, Address birthPlace, Date birthDate, Email email, PersonID mother, PersonID father) {
        this.personID = new PersonID(email);
        this.name = name;
        this.birthPlace = birthPlace;
        this.birthDate = birthDate;
        this.mother = mother;
        this.father = father;
        this.siblings = new HashSet<>();
        this.categories = new HashSet<>();
    }

    public Name getName() {
        return this.name;
    }

    public Address getBirthPlace() {
        return birthPlace;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public PersonID getPersonID() {
        return personID;
    }

    public PersonID getMother() {
        return this.mother;
    }

    public PersonID getFather() {
        return this.father;
    }

    public Set<Category> getCategories() {
        return new HashSet<>(categories);
    }

    /**
     * US001 - Know if a person is a sibling of another
     *
     * @param otherPerson
     * @return true if person is sibling
     */
    public boolean isSibling(Person otherPerson) {
        boolean sibling = false;

        if (otherPerson == null) {
            throw new NullPointerException("The person cannot be null");
        }

        boolean checkMotherAndFatherNull = (this.mother != null && otherPerson.mother != null && this.father != null && otherPerson.father != null);
        if (checkMotherAndFatherNull && (this.mother.equals(otherPerson.mother) || this.father.equals(otherPerson.father))) {
            sibling = true;
        }
        if (siblings.contains(otherPerson.getPersonID()) || otherPerson.siblings.contains(this.getPersonID())) {
            sibling = true;
        }

        return sibling;
    }

    /**
     * Adds an object of type Person to a collection of Persons that represent Siblings
     *
     * @param siblingID the object to be added
     */
    public void addSibling(PersonID siblingID) {
        if (siblingID == null) {
            throw new NullPointerException("The person ID cannot be null");
        }
        this.siblings.add(siblingID);
    }

    /**
     * Adds an object of type Category to the collection of Categories
     *
     * @param category
     */
    public void addCategory(Category category) {
        if (category == null) {
            throw new NullPointerException("Category cannot be null");
        }
        this.categories.add(category);
    }

    /**
     * Return a collection of Person representing Siblings
     *
     * @return this.siblings Collection of siblings
     */
    public Set<PersonID> getSiblings() {
        return new HashSet<>(this.siblings);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person person = (Person) o;
        return Objects.equals(personID, person.personID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personID);
    }

    @Override
    public String toString() {
        return "Person{" +
                "personID=" + personID +
                ", name=" + name +
                ", birthPlace=" + birthPlace +
                ", birthDate=" + birthDate +
                ", mother=" + mother +
                ", father=" + father +
                ", siblings=" + siblings +
                ", categories=" + categories +
                '}';
    }

    /**
     * Roots compare by identity
     *
     * @param other
     * @return
     */
    @Override
    public boolean sameIdentityAs(Person other) {
        return this.personID.sameValueAs(other.personID);
    }
}