package project.model.framework;

public interface Entity<T> {

    /**
     * Entities compare by identity, not by attributes.
     * @param other The Other entity.
     * @return true, if identities are the same, regardless of other attributes.
     */
    boolean sameIdentityAs(T other);
}

