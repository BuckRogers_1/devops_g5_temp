package project.model.framework;

public interface Root<T> {

    /**
     * Roots compare by identity
     *
     * @param other
     * @return
     */
    boolean sameIdentityAs(T other);
}
