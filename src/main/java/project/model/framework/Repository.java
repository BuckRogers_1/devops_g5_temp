package project.model.framework;

import java.util.Optional;

public interface Repository<T, K> {
    /**
     * Saves an entity either by creating it or updating it in the persistence
     * store.The reference is no
     * longer valid and use the returned object instead. e.g.,
     *
     * @param entity
     * @return the object reference to the persisted object.
     */
    <S extends T> S save(S entity);

    /**
     * Gets all entities from the repository.
     *
     * @return all entities from the repository
     */
    Iterable<T> findAll();

    /**
     * Gets the entity with the specified primary key.
     *
     * @param id
     * @return the entity with the specified primary key
     */
    Optional<T> findById(K id);
}