package project.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.time.Clock;

@Component
public class ClockUtil {

    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }
}
