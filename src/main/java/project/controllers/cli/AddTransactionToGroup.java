package project.controllers.cli;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.dto.AddTransactionToGroupRequestDTO;
import project.dto.TransactionDTO;
import project.dto.assemblers.AddTransactionToGroupRequestAssembler;
import project.services.AddTransactionToGroupService;

@Component
public class AddTransactionToGroup {

    @Autowired
    private AddTransactionToGroupService service;

    public TransactionDTO addTransactionToGroup(String memberID, String groupID, Double amount, String description, String category, String debit, String credit, String type) {
        AddTransactionToGroupRequestDTO addTransactionToGroupRequestDTO = AddTransactionToGroupRequestAssembler.mapToDTO(memberID, groupID, amount, description, category, debit, credit, type);

        return service.addTransactionToGroup(addTransactionToGroupRequestDTO);

    }
}
