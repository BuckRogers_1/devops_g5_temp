package project.controllers.cli;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.dto.AccountDTO;
import project.dto.AddAccountToGroupRequestDTO;
import project.dto.assemblers.AddAccountToGroupRequestAssembler;
import project.services.AddAccountToGroupService;

@Component
public class AddAccountToGroupController {
    @Autowired
    private AddAccountToGroupService service;

    /**
     * Finds group and person on repository and adds new account if person is responsible
     *
     * @param denomination
     * @param description
     * @param groupDescription
     * @param responsibleEmail
     * @return
     */
    public AccountDTO addAccountToGroup(String denomination, String description, String groupDescription, String responsibleEmail) {
        AddAccountToGroupRequestDTO addAccountToGroupRequestDTO = AddAccountToGroupRequestAssembler.mapToDTO(denomination,
                description,
                groupDescription,
                responsibleEmail);

        return service.addAccountToGroup(addAccountToGroupRequestDTO);
    }
}
