package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.AccountDTO;
import project.dto.AccountResponseDTO;
import project.dto.AddAccountToPersonRequestDTO;
import project.dto.AddAccountToPersonRequestInfoDTO;
import project.dto.assemblers.AccountResponseAssembler;
import project.dto.assemblers.AddAccountToPersonRequestAssembler;
import project.services.AddAccountToPersonService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping
public class AddAccountToPersonRestController {
    @Autowired
    private AddAccountToPersonService service;

    @PostMapping("/persons/{id}/accounts")
    public ResponseEntity<Object> addAccountToPerson(@PathVariable String id, @RequestBody AddAccountToPersonRequestInfoDTO info) {
        AddAccountToPersonRequestDTO addAccountToPersonRequestDTO = AddAccountToPersonRequestAssembler.addAccountMapToDTO(info.getDenomination(),
                info.getDescription(),
                id);

        AccountDTO accountDTO = service.addAccountToPerson(addAccountToPersonRequestDTO);

        AccountResponseDTO addAccountToPersonResponseDTO = AccountResponseAssembler.mapToDTO(accountDTO);

        Link selfLink = linkTo(AddAccountToPersonRestController.class).slash("accounts").slash(addAccountToPersonResponseDTO.getDenomination()).withSelfRel();
        Link personLink = linkTo(AddAccountToPersonRestController.class).slash("persons").slash(addAccountToPersonResponseDTO.getOwnerID()).withRel("person");
        addAccountToPersonResponseDTO.add(selfLink);
        addAccountToPersonResponseDTO.add(personLink);

        return new ResponseEntity<>(addAccountToPersonResponseDTO, HttpStatus.CREATED);
    }
}
