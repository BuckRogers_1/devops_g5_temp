package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.dto.AddMemberRequestDTO;
import project.dto.GroupDTO;
import project.dto.GroupResponseDTO;
import project.dto.assemblers.AddMemberRequestAssembler;
import project.dto.assemblers.AddMemberResponseAssembler;
import project.services.AddMemberToGroupService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;


@RestController
@RequestMapping
public class AddMemberToGroupRestController {
    @Autowired
    private AddMemberToGroupService service;

    /**
     * As a system manager, I want to add people to the group.
     *
     * @param
     * @return
     */
    @PostMapping("/groups/{groupDescription}/members/{personEmail}")
    public ResponseEntity<Object> addMemberToGroup(@PathVariable String groupDescription, @PathVariable String personEmail) {
        AddMemberRequestDTO addMemberRequestDTO = AddMemberRequestAssembler.mapToDTO(groupDescription, personEmail);

        GroupDTO groupDTO = service.addMember(addMemberRequestDTO);

        GroupResponseDTO groupResponseDTO = AddMemberResponseAssembler.mapToDTO(groupDTO);

        Link selfLink = linkTo(AddMemberToGroupRestController.class).slash("groups").slash(groupDescription).slash("members").withSelfRel();
        groupResponseDTO.add(selfLink);

        return new ResponseEntity<>(groupResponseDTO, HttpStatus.CREATED);
    }
}
