package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.AddCategoryRequestDTO;
import project.dto.AddCategoryRequestInfoDTO;
import project.dto.CategoryDTO;
import project.dto.CategoryResponseDTO;
import project.dto.assemblers.AddCategoryRequestAssembler;
import project.dto.assemblers.AddCategoryResponseAssembler;
import project.services.AddCategoryToGroupService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping
public class AddCategoryToGroupRestController {
    @Autowired
    private AddCategoryToGroupService service;

    @PostMapping("groups/{groupId}/categories")
    public ResponseEntity<Object> addCategoryToGroup(@RequestBody AddCategoryRequestInfoDTO info, @PathVariable String groupId) {
        AddCategoryRequestDTO addCategoryRequestDTO = AddCategoryRequestAssembler.addCategoryMapToDTO(info.getDesignation(),
                groupId,
                info.getPersonID());

        CategoryDTO categoryDTO;

        categoryDTO = service.addCategoryToGroup(addCategoryRequestDTO);

        CategoryResponseDTO categoryResponseDTO = AddCategoryResponseAssembler.mapToCategoryResponseDTO(
                categoryDTO);

        Link selfLink = linkTo(AddCategoryToGroupRestController.class).slash("groups").slash(groupId).slash("categories").slash(categoryResponseDTO.getDesignation()).withSelfRel();
        categoryResponseDTO.add(selfLink);

        return new ResponseEntity<>(categoryResponseDTO, HttpStatus.CREATED);
    }
}