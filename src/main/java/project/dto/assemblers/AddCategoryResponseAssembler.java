package project.dto.assemblers;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import project.dto.CategoryDTO;
import project.dto.CategoryResponseDTO;

@JsonSerialize
public class AddCategoryResponseAssembler {

    protected AddCategoryResponseAssembler() {
        throw new AssertionError();
    }

    public static CategoryResponseDTO mapToCategoryResponseDTO(CategoryDTO categoryDTO) {
        return new CategoryResponseDTO(categoryDTO.getDesignation().getDesignation());
    }
}
