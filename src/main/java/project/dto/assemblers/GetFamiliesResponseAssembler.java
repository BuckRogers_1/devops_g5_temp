package project.dto.assemblers;

import project.dto.GetFamiliesResponseDTO;
import project.dto.GroupDTO;

/**
 * Assembler for GetFamiliesResponseDTO
 */
public class GetFamiliesResponseAssembler {

    protected GetFamiliesResponseAssembler() {
        throw new AssertionError();
    }

    public static GetFamiliesResponseDTO mapToDTO(GroupDTO groupDTO) {
        return new GetFamiliesResponseDTO(groupDTO.getGroupID().getDescription().toString());
    }
}
