package project.dto.assemblers;

import project.dto.GroupDTO;
import project.dto.GroupResponseDTO;

public class AddMemberResponseAssembler {
    protected AddMemberResponseAssembler() {
        throw new AssertionError();
    }

    public static GroupResponseDTO mapToDTO(GroupDTO groupDTO) {
        return new GroupResponseDTO(groupDTO.getGroupID().getDescription().getDescription());
    }
}
