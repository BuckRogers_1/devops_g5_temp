package project.dto.assemblers;

import project.dto.AddAccountToPersonRequestDTO;
import project.model.shared.Denomination;
import project.model.shared.Description;
import project.model.shared.Email;
import project.model.shared.PersonID;

public class AddAccountToPersonRequestAssembler {

    protected AddAccountToPersonRequestAssembler() {
        throw new AssertionError();
    }

    /**
     * This will mapTo an EntryDTO with information from infoDTO (from web) and transform it usable within CLI
     *
     * @param denomination String that came from JSON
     * @param description  String that came from JSON
     * @param personEmail  String that came from JSON
     * @return AccountEntryDTO
     */
    public static AddAccountToPersonRequestDTO addAccountMapToDTO(String denomination, String description, String personEmail) {
        Denomination accDenomination = new Denomination(denomination);
        Description accDescription = new Description(description);
        PersonID pID = new PersonID(new Email(personEmail));

        return new AddAccountToPersonRequestDTO(accDenomination, accDescription, pID);
    }
}
