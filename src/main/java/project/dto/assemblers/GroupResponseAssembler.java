package project.dto.assemblers;

import project.dto.GroupResponseDTO;

public class GroupResponseAssembler {

    protected GroupResponseAssembler() {
        throw new AssertionError();
    }

    public static GroupResponseDTO mapToDTO(String groupDescription) {
        return new GroupResponseDTO(groupDescription);
    }
}
