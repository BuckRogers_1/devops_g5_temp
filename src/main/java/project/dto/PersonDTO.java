package project.dto;

import project.model.shared.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class PersonDTO {
    private PersonID personID;
    private Name name;
    private Address birthPlace;
    private Date birthDate;
    private PersonID mother;
    private PersonID father;
    private Set<PersonID> siblings;
    private Set<Category> categories;

    public PersonDTO() {
    }

    public PersonID getPersonID() {
        return personID;
    }

    public void setPersonID(PersonID personID) {
        this.personID = personID;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Address getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(Address birthPlace) {
        this.birthPlace = birthPlace;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public PersonID getMother() {
        return mother;
    }

    public void setMother(PersonID mother) {
        this.mother = mother;
    }

    public PersonID getFather() {
        return father;
    }

    public void setFather(PersonID father) {
        this.father = father;
    }

    public Set<PersonID> getSiblings() {
        return new HashSet<>(siblings);
    }

    public void setSiblings(Set<PersonID> siblings) {
        this.siblings = new HashSet<>(siblings);
    }

    public Set<Category> getCategories() {
        return new HashSet<>(categories);
    }

    public void setCategories(Set<Category> categories) {
        this.categories = new HashSet<>(categories);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonDTO)) return false;
        PersonDTO personDTO = (PersonDTO) o;
        return Objects.equals(personID, personDTO.personID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personID, name, birthPlace, birthDate, mother, father, siblings, categories);
    }
}