package project.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

/**
 * DTO Class that is used when responding to a GetFamilies request
 */
public class GetFamiliesResponseDTO extends RepresentationModel<GetFamiliesResponseDTO> {
    private String group;

    public GetFamiliesResponseDTO(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GetFamiliesResponseDTO that = (GetFamiliesResponseDTO) o;
        return Objects.equals(group, that.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(group);
    }

    @Override
    public @NotNull String toString() {
        return group;
    }
}
