package project.dto;

import project.model.shared.*;

import java.util.Objects;

public class AddTransactionToGroupRequestDTO {
    private PersonID personID;
    private GroupID groupID;
    private double amount;
    private Description description;
    private Category category;
    private AccountID debit;
    private AccountID credit;
    private TransactionType type;


    public AddTransactionToGroupRequestDTO(PersonID personID, GroupID groupID, double amount, Description description, Category category, AccountID debit, AccountID credit, TransactionType type) {
        this.personID = personID;
        this.groupID = groupID;
        this.amount = amount;
        this.description = description;
        this.category = category;
        this.debit = debit;
        this.credit = credit;
        this.type = type;
    }

    public PersonID getPersonID() {
        return personID;
    }

    public void setPersonID(PersonID personID) {
        this.personID = personID;
    }

    public GroupID getGroupID() {
        return groupID;
    }

    public void setGroupID(GroupID groupID) {
        this.groupID = groupID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public AccountID getDebit() {
        return debit;
    }

    public void setDebit(AccountID debit) {
        this.debit = debit;
    }

    public AccountID getCredit() {
        return credit;
    }

    public void setCredit(AccountID credit) {
        this.credit = credit;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddTransactionToGroupRequestDTO that = (AddTransactionToGroupRequestDTO) o;
        return Objects.equals(personID, that.personID) &&
                Objects.equals(groupID, that.groupID) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(description, that.description) &&
                Objects.equals(category, that.category) &&
                Objects.equals(debit, that.debit) &&
                Objects.equals(credit, that.credit) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(personID, groupID, amount, description, category, debit, credit, type);
    }
}