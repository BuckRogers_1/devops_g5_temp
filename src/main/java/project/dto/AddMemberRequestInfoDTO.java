package project.dto;

import java.util.Objects;

public class AddMemberRequestInfoDTO {
    String groupDescription;
    String personEmail;

    public AddMemberRequestInfoDTO(String groupDescription, String personEmail) {
        this.groupDescription = groupDescription;
        this.personEmail = personEmail;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getPersonEmail() {
        return personEmail;
    }

    public void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddMemberRequestInfoDTO that = (AddMemberRequestInfoDTO) o;
        return Objects.equals(groupDescription, that.groupDescription) &&
                Objects.equals(personEmail, that.personEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupDescription, personEmail);
    }
}
