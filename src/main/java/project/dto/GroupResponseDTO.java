package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class GroupResponseDTO extends RepresentationModel<GroupResponseDTO> {
    private String groupDescription;

    public GroupResponseDTO(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GroupResponseDTO that = (GroupResponseDTO) o;
        return Objects.equals(groupDescription, that.groupDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupDescription);
    }

}
