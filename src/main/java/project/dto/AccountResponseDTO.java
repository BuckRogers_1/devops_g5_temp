package project.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

@JsonSerialize
public class AccountResponseDTO extends RepresentationModel<AccountResponseDTO> {
    private String denomination;
    private String description;
    private String ownerID;

    public AccountResponseDTO(String denomination, String description, String ownerID) {
        this.denomination = denomination;
        this.description = description;
        this.ownerID = ownerID;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountResponseDTO that = (AccountResponseDTO) o;
        return Objects.equals(denomination, that.denomination) &&
                Objects.equals(description, that.description) &&
                Objects.equals(ownerID, that.ownerID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(denomination, description, ownerID);
    }
}
