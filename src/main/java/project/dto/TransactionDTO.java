package project.dto;

import project.model.shared.*;

import java.util.Objects;

public class TransactionDTO {
    private Double amount;
    private Description description;
    private TransactionDate date;
    private Category category;
    private AccountID debit;
    private AccountID credit;
    private TransactionType type;

    public TransactionDTO() {
    }

    public TransactionDTO(Double amount, Description description, TransactionDate date, Category category, AccountID debit, AccountID credit, TransactionType type) {
        this.amount = amount;
        this.description = description;
        this.date = date;
        this.category = category;
        this.debit = debit;
        this.credit = credit;
        this.type = type;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public TransactionDate getDate() {
        return date;
    }

    public void setDate(TransactionDate date) {
        this.date = date;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public AccountID getDebit() {
        return debit;
    }

    public void setDebit(AccountID debit) {
        this.debit = debit;
    }

    public AccountID getCredit() {
        return credit;
    }

    public void setCredit(AccountID credit) {
        this.credit = credit;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionDTO that = (TransactionDTO) o;
        return Objects.equals(amount, that.amount) &&
                Objects.equals(description, that.description) &&
                Objects.equals(date, that.date) &&
                Objects.equals(category, that.category) &&
                Objects.equals(debit, that.debit) &&
                Objects.equals(credit, that.credit) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, description, date, category, debit, credit, type);
    }
}
