package project.dto;

import java.util.Objects;

public class AddTransactionToGroupRequestInfoDTO {
    private String personID;
    private String groupID;
    private double amount;
    private String description;
    private String category;
    private String debit;
    private String credit;
    private String type;

    public AddTransactionToGroupRequestInfoDTO(String personID, String groupID, double amount, String description, String category, String debit, String credit, String type) {
        this.personID = personID;
        this.groupID = groupID;
        this.amount = amount;
        this.description = description;
        this.category = category;
        this.debit = debit;
        this.credit = credit;
        this.type = type;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddTransactionToGroupRequestInfoDTO that = (AddTransactionToGroupRequestInfoDTO) o;
        return Objects.equals(personID, that.personID) &&
                Objects.equals(groupID, that.groupID) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(description, that.description) &&
                Objects.equals(category, that.category) &&
                Objects.equals(debit, that.debit) &&
                Objects.equals(credit, that.credit) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personID, groupID, amount, description, category, debit, credit, type);
    }
}




